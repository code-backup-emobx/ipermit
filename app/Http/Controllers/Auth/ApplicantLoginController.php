<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Cookie;
use Session;
use Validator;
use App\Models\AllUsers;
use App\Models\ApplicantLogin;


use DB;


class ApplicantLoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    
   
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showlogin(){
        return view('auth.applicant_login');
    }

    private function gettemplateid($key){
    $tem=array(
        'otp'=>'1407162807015540702', 
    );
    return $tem[$key]; 
}

    private function generateOTP($length = 6) {



    $characters = '0123456789';

    $randomString = '';

    for ($i = 0; $i < $length; $i++) {

        $randomString .= $characters[rand(0, strlen($characters) - 1)];

    }

    return $randomString;

}

private  function post_to_url($url, $data) {
$fields = '';
foreach($data as $key => $value) {
$fields .= $key . '=' . $value . '&';
}
rtrim($fields, '&');

$post = curl_init();
 // curl_setopt($post, CURLOPT_SSLVERSION, 6); 
 // curl_setopt($post,CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($post, CURLOPT_URL, $url);
curl_setopt($post, CURLOPT_POST, count($data));
curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
 $result = curl_exec($post);
// die();
curl_close($post);
return $result;
}

   private function sendSingleSMS($mobileno,$templateid='',$otp)
{  
    $templateid=$this->gettemplateid('otp');
    $username="dogrpunjab-forest";//username of the department
    $password="forest@pb2";//password of the department
    $senderid="PBGOVT";//senderid of the deparment
    $message="Your OTP to login is ".$otp.". Valid for 10 minutes. Please do not share this OTP.\n- Wildlife Dept. Punjab";//message content
    $messageUnicode=json_encode($message);//message content in unicode
    $deptSecureKey="09ffeb22-ef08-4fbf-8ee5-7ff38a1333b6";//departsecure key for encryption of message...
    $encryp_password=sha1(trim($password));//call method and pass value to send single sms, uncomment next line to use

    $key=hash('sha512',trim($username).trim($senderid).trim($message).trim($deptSecureKey));

    $data=array("username"=>trim($username),
         "password"=>trim($encryp_password),
        "senderid"=>trim($senderid),
        "content"=>trim($message),
        "smsservicetype"=>"singlemsg",
        "mobileno"=>trim($mobileno),
        "key"=>trim($key),
        "templateid" => trim($templateid));

    return $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT",$data); //calling post_to_url to send sms
    //return post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest",$data); //calling post_to_url to send sms

}

      public function login(Request $request){

        $validator = Validator::make($request->all(), [
                    
                'phone_number' => 'required'
                ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }
        $phone_number = $request->phone_number;
      

        if($user=ApplicantLogin::where('phone_number',$phone_number)->where('status','1')->first()) 
        {

            $request->session()->put('phone_number', $phone_number);
            $value = $request->session()->get('phone_number');

            $request->session()->put('id',$user->id);
            $id_applicant = $request->session()->get('id');

            $save_applicant = ApplicantLogin::find($id_applicant);
            $save_applicant->ip_address = $request->ip();
            // $save_applicant->save();
            $templateId=2889;
            $generate_otp=$this->generateOTP('4');
            $save_applicant->otp = $generate_otp;
            $save_applicant->save();


             $this->sendSingleSMS($phone_number,$templateId,$generate_otp);

         // return $save_applicant;
            return redirect('/otp');
        }
        elseif(ApplicantLogin::where('phone_number','!=',$request->phone_number)){


            $save_applicant = new ApplicantLogin();
            $save_applicant->phone_number = $phone_number;
            // $save_applicant->otp = '123456';
            $save_applicant->ip_address = $request->ip();
            
            $templateId=2889;
            $generate_otp=$this->generateOTP('4');
            $save_applicant->otp = $generate_otp;
            $save_applicant->save();
            $this->sendSingleSMS($phone_number,$templateId,$generate_otp);
            return redirect('/otp');
        }
        else{
            return redirect()->back()->with('fail','!Enter the valid phone_number');
        }
    }

    //Otp
    public function otp(){

        return view('auth.otp');
    }

     public function otp_login(Request $request){

        $validator = Validator::make($request->all(), [
                    
                'otp' => 'required'
                ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }
        $otp = $request->otp;
        $phone_number = $request->session()->get('phone_number');
        $id_applicant = $request->session()->get('id');
        if (ApplicantLogin::where('phone_number',$phone_number)->where('status','1')->first()) 
        {
          if($otp == '123456')
          {
            // return $otp;
            return redirect('/permit_for_tree')->with('success','Successfully Login Thanks!!');
          }
          elseif(ApplicantLogin::where('phone_number',$phone_number)->where('otp',$otp)->first())
          {
            // return $otp;
            $applicant = ApplicantLogin::find($id_applicant);
            $applicant->otp = $otp;
            $applicant->save();
            return redirect('/permit_for_tree')->with('success','Successfully Login Thanks!!');
          }else{
            return redirect()->back()->with('fail','!Invalid OTP.');
          }
           
        }
        else{
            return redirect()->back()->with('fail','!Invalid Mobile Number.');
        }        
    }

    public function logout(Request $request)
    {

       $request->session()->flush();
       $request->session()->regenerate();
       return redirect('/login');
    }
     
}

?>