<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\AllUsers;
use App\Models\PermitForHunting;
use App\Models\PermitForTree;
use App\Models\RoSurvey;
use App\Models\PerfomaHunters;
use App\Models\TehsilDistrictDivision;
use App\Models\applicationStatus;
use App\Models\RejectApplicantPermit;
use App\Models\ForestDivision;
use App\Models\ForestRange;
use App\Models\Respond;
use App\Models\Survey;
use App\Models\QuestionTables;
use App\Models\AnswerTables;
use App\Models\OwnerDetails;
use App\Models\WeaponDetails;
use App\Models\UserSpeciescount;
use App\Models\SarpunchAddress;
use App\Models\Hunters;
use PDF;

class AdminDashboardController extends Controller
{
	public function admin(Request $request)
	{
		// return $request->all();
		$search=$request->search;
        $from = $request->from;
        $to = $request->to;
        if ($from != '' OR $to != '')
	    {
	        $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil')->where('applicant_name', 'like', '%'.$search.'%')->whereDate('applied_date','>=',$from)->whereDate('applied_date','<=', $to)->get();
	    }elseif($search !='')
	    {
	    	$applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil')->where('applicant_name', 'like', '%'.$search.'%')->get();
	    }
	    else{

		  $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil')->get();
	    }
		
		// return $applicant_detail_for_cf; die;
		return view('adminLogin.home',compact('applicant_detail_for_cf','search','from','to'));
	}

	public function cf_home(Request $request){
		$data=array();
		
		$search=$request->search;
        $from = $request->from;
        $to = $request->to;
        // return Auth::user()->name;
        if(Auth::user()->name == 'CF_Shivalik')
        {
	        if ($from != '' OR $to != '')
		    {
		        $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')->where('applicant_name', 'like', '%'.$search.'%')
		        ->whereDate('applied_date','>=',$from)
		        ->whereDate('applied_date','<=', $to)
		        ->whereIn('division_id',['1','2','3'])->get();
		    }elseif($search !='')
		    {
		    	$applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')
		    	->where('applicant_name', 'like', '%'.$search.'%')
		    	->whereIn('division_id',['1','2','3'])->get();
		    }
		    else{

			  $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')
			  ->whereIn('division_id',['1','2','3'])->get();
		    }
		}elseif(Auth::user()->name == 'CF_North_Circle')
		{
			if ($from != '' OR $to != '')
		    {
		        $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')->where('applicant_name', 'like', '%'.$search.'%')
		        ->whereDate('applied_date','>=',$from)
		        ->whereDate('applied_date','<=', $to)
		        ->whereIn('division_id',['4','5'])->get();
		    }elseif($search !='')
		    {
		    	$applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')
		    	->where('applicant_name', 'like', '%'.$search.'%')
		    	->whereIn('division_id',['4','5'])->get();
		    }
		    else{

			  $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')
			  ->whereIn('division_id',['4','5'])->get();
		    }
		}elseif(Auth::user()->name == 'CCF_Hills_Zone')
		{
			if ($from != '' OR $to != '')
		    {
		        $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')->where('applicant_name', 'like', '%'.$search.'%')
		        ->whereDate('applied_date','>=',$from)
		        ->whereDate('applied_date','<=', $to)
		        ->whereIn('division_id',['1','2','3','4','5'])->get();
		    }elseif($search !='')
		    {
		    	$applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')
		    	->where('applicant_name', 'like', '%'.$search.'%')
		    	->whereIn('division_id',['1','2','3','4','5'])->get();
		    }
		    else{

			  $applicant_detail_for_cf = PermitForTree::with('getDivision','getTehsil','applicationStatus.getRange')
			  ->whereIn('division_id',['1','2','3','4','5'])->get();
		    }
		}
		// return $applicant_detail_for_cf; 
		return view('adminLogin.cf',compact('applicant_detail_for_cf','search','from','to'));
	}

	

	public function adminDashboard(Request $request){
		$data=array();
		$status=$request->status;
		$division_id = Auth::user()->division_id;
		if($status == 'last_7_days')
		{
			$date = \Carbon\Carbon::today()->subDays(7);
			$applicant_data_for_dfo=PermitForTree::with('getDivision','getTehsil')
	    	->where('division_id',$division_id)
	    	->where('applied_date', '>=', $date)->get();
		}
	    elseif($status == 'Pending')
	    {
	    	$applicant_data_for_dfo = PermitForTree::with('getDivision','getTehsil')
	    	->where('division_id',$division_id)
	    	->whereIn('dfo_status',['Pending'])->get();
	    }
	    else
	    {
	    	$applicant_data_for_dfo = PermitForTree::with('getDivision','getTehsil')->where('division_id',$division_id)->get();
	    }
		
		// return $x = PermitForHunting::get();die;
		
		
		// $applicant_data_for_dfo = PermitForHunting::join('application_status', 'applicant_permit_id', 'permit_for_huntings.id')
		// ->where('permit_for_huntings.division_id',$division_id)
		// ->get();

	 	// return array('data'=>$applicant_data_for_dfo); 
	 	$data['range'] = ForestRange::where('division_id',$division_id)->get();
        $data['applicant_data_for_dfo']=$applicant_data_for_dfo;
        // echo '<pre>';
        // print_r($applicant_data_for_dfo);
        //  echo '</pre>'; die();
	    return view('adminLogin.dashboard',$data);
	}

	public function ro_dashboard(Request $request)
	{
		$data=array();
		$status=$request->status;
		$range_name = Auth::user()->name;
		$range = ForestRange::where('range_name',$range_name)->first();
		if($status == 'last_7_days')
		{
			$date = \Carbon\Carbon::today()->subDays(7);
			$applicant_data_for_ro = PermitForTree::join('application_status', 'applicant_permit_id', 'permit_for_tree.id')
			->where('application_status.range_id',$range->id)
			->where('permit_for_tree.dfo_status','Accepted')
			->where('permit_for_tree.applied_date', '>=', $date)->get();
		}
	    elseif($status == 'Pending')
	    {
	    	$applicant_data_for_ro = PermitForTree::join('application_status', 'applicant_permit_id', 'permit_for_tree.id')
			->where('application_status.range_id',$range->id)
			->where('permit_for_tree.dfo_status','Accepted')
			->where('application_status.ro_status','pending')->get();
	    }
	    else
	    {
	    	$applicant_data_for_ro = PermitForTree::join('application_status', 'applicant_permit_id', 'permit_for_tree.id')
			->where('application_status.range_id',$range->id)
			->where('permit_for_tree.dfo_status','Accepted')
			->where('application_status.dfo_status','Accepted')->get();
	    }
		
	   

	   // return $applicant_data_for_ro; die;
        $data['applicant_data_for_ro']=$applicant_data_for_ro;
	  return view('adminLogin.rohome',$data);
	}

	public function show(Request $request , $id){

		$data=array();
		$applicant_detail_for_dfo = PermitForTree::with('getDivision','applicationStatus')->where('id',$id)->first();
		
		$question_list=QuestionTables::where('role', 'cf')->get();
		$data['question_list']=$question_list;
		$owner_details=OwnerDetails::where('applicant_permit_id', $id)->get();
		
		$request->session()->put('permit_id', $applicant_detail_for_dfo->id);
        $applicant_permit_id = $request->session()->get('permit_id');
        $request->session()->put('applicant_id', $applicant_detail_for_dfo->applicant_id);
        $applicant_id = $request->session()->get('applicant_id');

        $data['applicant_detail_for_dfo']=$applicant_detail_for_dfo;
        $data['owner_details']=$owner_details;
		return view('cfView.show',$data);
	}

	public function viewApplicantDetail(Request $request , $id){

		$division_id = Auth::user()->division_id;
		
		$data=array();
		$applicant_detail_for_dfo = PermitForTree::with('getDivision','applicationStatus')->where('id',$id)->first();
		$owner_details=OwnerDetails::where('applicant_permit_id', $id)->get();
        
		
		$request->session()->put('permit_id', $applicant_detail_for_dfo->id);
        $applicant_permit_id = $request->session()->get('permit_id');
        $request->session()->put('applicant_id', $applicant_detail_for_dfo->applicant_id);
        $applicant_id = $request->session()->get('applicant_id');

        $question_list=QuestionTables::where('role', 'dfo')->get();

        $data['range'] = ForestRange::where('division_id',$division_id)->get();
        $data['applicant_detail_for_dfo']=$applicant_detail_for_dfo;
        $data['question_list']=$question_list;
        $data['owner_details']=$owner_details;
        
    
		return view('dfoView.applicant_detail',$data);
	}

	public function viewApplicantDetailby_Ro(Request $request , $id){

		$division_id = Auth::user()->division_id;
		$data=array();
		// return $division_id;
		// $applicant_detail_for_dfo = PermitForHunting::with('getDivision','getTehsil')->where('id',$id)->orWhere('division_id',$division_id)->get();
		$applicant_detail_for_ro = PermitForTree::with('getDivision','getTehsil','applicationStatus')->where('id',$id)->first();
		
		
		$question_list=QuestionTables::where('role', 'ro')->get();

        $owner_details=OwnerDetails::where('applicant_permit_id', $id)->get();

		$request->session()->put('permit_id', $applicant_detail_for_ro->id);
        $applicant_permit_id = $request->session()->get('permit_id');
        $request->session()->put('applicant_id', $applicant_detail_for_ro->applicant_id);
        $applicant_id = $request->session()->get('applicant_id');
        // dd($applicant_id); die();
        // $data['range'] = ForestRange::where('range_name',$range_name)->first();
        $data['applicant_detail_for_ro']=$applicant_detail_for_ro;
        $data['question_list']=$question_list;
        $data['owner_details']=$owner_details;
		return view('roView.applicant_detail',$data);
	}

	public function application_status(Request $request){
         // return $request->all();
		// $validator = Validator::make($request->all(), [
                    
  //           'range_id' => 'required'
  //       ]);

  //           if ($validator->fails()) 
  //           {
  //               return redirect()
  //                       ->back()
  //                       ->withErrors($validator)
  //                       ->withInput();
  //           }
        $division_id = Auth::user()->division_id;
		
		
		$dfo_status = $request->input('dfo_status');
		$ro_status = $request->input('ro_status');
		
		
		
		$applicant_permit_id = $request->session()->get('permit_id');
		$applicant_id = $request->session()->get('applicant_id');
		$role_by=Auth::user()->role;
        $dfo_sign='';
        
		if($ro_status == 'dfo')
		{
			$validator = Validator::make($request->all(), [
                    
	            'question.*' => 'required',
	            'species_count.*' => 'required',
	        ]);

	        if ($validator->fails()) 
	        {
	            return redirect()
	                    ->back()
	                    ->withErrors($validator)
	                    ->withInput();
	        }
			$question = $request->input('question');
			$species_count = $request->input('species_count');
			$species_id = $request->input('species_id');
	        foreach ($question as $key => $value) 
	        {
	        	$save_answer = new AnswerTables();
	        	$save_answer->applicant_permit_id = $applicant_permit_id;
	        	$save_answer->question_id = $key;
	        	$save_answer->answer = $value;
	        	$save_answer->role_by = $role_by;
	        	$save_answer->save();
	        }
            $answer_tables=AnswerTables::where('applicant_permit_id', $applicant_permit_id)->get();
            $answer_ids=array();
            foreach ($answer_tables as $answer_list) 
            {
            	$answer_ids[]= $answer_list->id;
            }
            $answer_id=implode(',', $answer_ids);
            
	        $save_survey = new Survey();
	        $save_survey->applicant_permit_id = $applicant_permit_id;
	        $save_survey->role_by = $role_by;
	        $save_survey->answer_id = $answer_id;
	        $save_survey->species_id = implode(',', $species_id);
	        $save_survey->species_count = implode(',', $species_count);
	        $save_survey->save();
		}
		if(applicationStatus::where('applicant_permit_id',$applicant_permit_id)->first()){

			$save_reason =applicationStatus::where('applicant_permit_id',$applicant_permit_id)->first();
			$old_range_id=$save_reason->range_id;

			$role_by=Auth::user()->role;
			if($role_by == 'ro')
			{
				$save_reason->applicant_id = $applicant_id;
				$save_reason->division_id = $division_id;
				$save_reason->applicant_permit_id = $applicant_permit_id;
				$save_reason->dfo_status = $dfo_status;
				$save_reason->ro_status = $ro_status;
				$save_reason->user_status = 'Approved by RO';
				// $save_reason->ip_action_dfo = $request->ip();
				$save_reason->role_by = Auth::user()->role;
				$save_reason->save();
			}elseif($role_by == 'dfo' && $dfo_status == 'ro')
			{
				$validator = Validator::make($request->all(), [
                    
		            'range_id' => 'required'
		        ]);

		            if ($validator->fails()) 
		            {
		                return redirect()
		                        ->back()
		                        ->withErrors($validator)
		                        ->withInput();
		            }
				$range_id = $request->input('range_id');
				$hunting_license_validity = $request->input('hunting_license_validity');
				$save_reason->applicant_id = $applicant_id;
				$save_reason->division_id = $division_id;
				$save_reason->range_id = $range_id;
				$save_reason->applicant_permit_id = $applicant_permit_id;
				$save_reason->dfo_status = $dfo_status;
				$save_reason->ro_status = 'pending';
				$save_reason->user_status = 'Assign To RO';
				$save_reason->hunting_license_validity = $hunting_license_validity;
				// $save_reason->ip_action_dfo = $request->ip();
				$save_reason->role_by = Auth::user()->role;
				$save_reason->save();
			}elseif($role_by == 'dfo' && $dfo_status == 'accepted')
			{
				// return $request->all();
				$validator = Validator::make($request->all(), [
		                    
		            'hunting_license_validity' => 'required',
		            'sign_image' => 'required|mimes:jpeg,png,jpg|max:300'
		        ]);

		            if ($validator->fails()) 
		            {
		                return redirect()
		                        ->back()
		                        ->withErrors($validator)
		                        ->withInput();
		            }
		        // return $request->file('sign_image');
				if($request->hasFile('sign_image') != "") {
		            $dfo_sign = $request->file('sign_image');
		            // $name=$dfo_sign->getClientOriginalName();   
		            // dd($name); 
		            // $dfo_sign->move(public_path().'/dfo_signature/', $name);

		            $filename = time() . '.' . $dfo_sign->getClientOriginalExtension();
		            // $filename =$dfo_sign->getClientOriginalName();
		            $destinationPath = public_path('/dfo_signature');
		            $dfo_sign->move($destinationPath, $filename);
		            $dfo_sign = '/dfo_signature/' . $filename;
		        }
		         // return $dfo_sign;
				$hunting_license_validity = $request->input('hunting_license_validity');
				$save_reason->applicant_id = $applicant_id;
				$save_reason->division_id = $division_id;
				$save_reason->applicant_permit_id = $applicant_permit_id;
				$save_reason->dfo_status = $dfo_status;
				$save_reason->hunting_license_validity = $hunting_license_validity;
				$save_reason->range_id = $old_range_id;
				$save_reason->user_status = 'Approved';
				$save_reason->role_by = Auth::user()->role;
				$save_reason->save();
			}
			$permit_status = PermitForHunting::find($applicant_permit_id);
			$permit_status->dfo_status = $save_reason->dfo_status;
			$permit_status->dfo_sign = $dfo_sign;
			$permit_status->save();
			
			return redirect()->back();
		}
		else{
            $range_id = $request->input('range_id');
			$save_reason = new applicationStatus();
			$save_reason->applicant_id = $applicant_id;
			$save_reason->division_id = $division_id;
			$save_reason->range_id = $range_id;
			$save_reason->applicant_permit_id = $applicant_permit_id;
			$save_reason->dfo_status = $dfo_status;
			$save_reason->user_status = 'Assign to RO';
			// $save_reason->ip_action_dfo = $request->ip();
			$save_reason->role_by = Auth::user()->role;
			// $save_reason->by_id = Auth::user()->id;
			$save_reason->status = 'active';
			$save_reason->save();
			$permit_status = PermitForHunting::find($applicant_permit_id);
			$permit_status->dfo_status = $save_reason->dfo_status;
			$permit_status->save();
			return redirect()->back();
		}
	}

	public function dfo_survey(Request $request)
    {
    	
    	$validator = Validator::make($request->all(), [
                    
            'question' => 'required',
            
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        // return $request->all();
    	$division_id = Auth::user()->division_id;
		
		$applicant_permit_id = $request->session()->get('permit_id');
		$applicant_id = $request->session()->get('applicant_id');
		$role_by=Auth::user()->role;
		$question = $request->input('question');
		
		
		$dfo_status='';
		$question_list=QuestionTables::where('role', 'dfo')->first();
        
    	if($question == 'yes')
    	{
    		$dfo_status='Accepted';
    		$user_status='Accepted by DFO & Mark to RO';
    		$range_id = $request->input('range_id');
    	}else
    	{
    		$dfo_status='Assign to CF';
    		$range_id = NULL;
    		$user_status='Assign to CF';
    	}

    	// return $dfo_status;
        	$save_answer = new AnswerTables();
        	$save_answer->applicant_permit_id = $applicant_permit_id;
        	$save_answer->question_id = $question_list->id;
        	$save_answer->answer = $question;
        	$save_answer->role_by = $role_by;
        	$save_answer->save();
        // if(applicationStatus::where('applicant_permit_id',$applicant_permit_id)->first()){
        // 	$save= applicationStatus::where('applicant_permit_id',$applicant_permit_id)->first();
        // 	$save->applicant_permit_id = $applicant_permit_id;
	       //  $save->applicant_id = $applicant_id;
	       //  $save->role_by = $role_by;
	       //  $save->division_id = $division_id;
	       //  $save->user_status = $dfo_status;
	       //  $save->dfo_status = $dfo_status;
	       //  $save->save();

	       //  $permit_status = PermitForTree::where('id', $applicant_permit_id)->first();
	       //  $permit_status->dfo_status=$dfo_status;
	       //  $permit_status->user_status=$dfo_status;
	       //  $permit_status->save();
	       //  return redirect()->back();
        // }
        // else{
        	$save = new applicationStatus();
	        $save->applicant_permit_id = $applicant_permit_id;
	        $save->applicant_id = $applicant_id;
	        $save->role_by = $role_by;
	        $save->division_id = $division_id;
	        $save->range_id = $range_id;
	        $save->user_status = $user_status;
	        $save->dfo_status = $dfo_status;
	        $save->status_date = date('Y-m-d');
	        // $save->cf_status = $cf_status;
	        $save->save();

	        $permit_status = PermitForTree::where('id', $applicant_permit_id)->first();
	        $permit_status->dfo_status=$dfo_status;
	        $permit_status->user_status=$user_status;
	        $permit_status->save();
	        return redirect()->back()->with('success','Successfully '.$dfo_status);
        // }
    }


    public function cf_survey(Request $request)
    {
    	
    	$validator = Validator::make($request->all(), [
                    
            'question' => 'required',
            
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        // return $request->all();
    	// return $division_id = Auth::user()->division_id;
		
		$applicant_permit_id = $request->session()->get('permit_id');
		$applicant_id = $request->session()->get('applicant_id');
		$role_by=Auth::user()->role;
		$question = $request->input('question');
		
		$permit_status = PermitForTree::where('id', $applicant_permit_id)->first();
		$division_id = $permit_status->division_id;

		$cf_status='';
		$question_list=QuestionTables::where('role', 'cf')->first();
        
    	if($question == 'yes')
    	{
    		$cf_status='Accepted';
    		$user_status='Accepted by CF';
    		
    	}else
    	{
    		$cf_status='Hold on CF';
    		
    		$user_status='Hold on CF';
    	}

    	// return Auth::user()->role;
        	$save_answer = new AnswerTables();
        	$save_answer->applicant_permit_id = $applicant_permit_id;
        	$save_answer->question_id = $question_list->id;
        	$save_answer->answer = $question;
        	$save_answer->role_by = Auth::user()->role;
        	$save_answer->save();
       	// return $save_answer;die;
        	$save = new applicationStatus();
	        $save->applicant_permit_id = $applicant_permit_id;
	        $save->applicant_id = $applicant_id;
	        $save->role_by = $role_by;
	        $save->division_id = $division_id;
	        // $save->range_id = $range_id;
	        $save->user_status = $user_status;
	        $save->cf_status = $cf_status;
	        $save->status_date = date('Y-m-d');
	        // $save->cf_status = $cf_status;
	        $save->save();

	        $permit_status = PermitForTree::where('id', $applicant_permit_id)->first();
	        $permit_status->cf_status=$cf_status;
	        $permit_status->user_status=$user_status;
	        $permit_status->save();
	        return redirect()->back()->with('success','Successfully '.$cf_status);
        
    }


    public function ro_survey(Request $request)
    {
    	
    	$validator = Validator::make($request->all(), [
                    
            'station_report' => 'required|max:2048',
            'hammering' => 'required',
            'kml' => 'required',
            
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        // return $request->all();
    	$division_id = Auth::user()->division_id;
		
		$applicant_permit_id = $request->session()->get('permit_id');
		$applicant_id = $request->session()->get('applicant_id');
		$role_by=Auth::user()->role;

		$range_name = Auth::user()->name;
		$range = ForestRange::where('range_name',$range_name)->first();


		$hammering = $request->input('hammering');


		$station_report='';
		if($request->hasFile('station_report') != ""){
        	$filename =$station_report->getClientOriginalName();
            $destinationPath = public_path('/station_report');
            $station_report->move($destinationPath, $filename);
            $station_report = '/station_report/' . $filename;
		}

		$kml='';
		if($request->hasFile('kml') != ""){
        	$filename =$kml->getClientOriginalName();
            $destinationPath = public_path('/kml');
            $kml->move($destinationPath, $filename);
            $kml = '/kml/' . $filename;
		}
		
		

    	// return $dfo_status;
        	$save = new RoSurvey();
        	$save->applicant_permit_id = $applicant_permit_id;
        	$save->applicant_id = $applicant_id;
        	$save->station_report= $station_report;
        	$save->kml = $kml;
        	$save->hammering = $hammering;
        	$save->role_by = $role_by;
        	$save->save();
        
        	$save = new applicationStatus();
	        $save->applicant_permit_id = $applicant_permit_id;
	        $save->applicant_id = $applicant_id;
	        $save->role_by = $role_by;
	        $save->division_id = $division_id;
	        $save->range_id = $range->id;
	        $save->status_date = date('Y-m-d');
	        $save->user_status = 'Accepted by RO & assign to DFO';
	        $save->dfo_status = 'Assign by RO';
	        $save->ro_status = 'Accepted by RO & assign to DFO';
	        // $save->cf_status = $cf_status;
	        $save->save();

	        $permit_status = PermitForTree::where('id', $applicant_permit_id)->first();
	        $permit_status->dfo_status='Assign by RO';
	        $permit_status->user_status='Accepted by RO & assign to DFO';
	        $permit_status->ro_status='Accepted by RO & assign to DFO';
	        $permit_status->save();
	        return redirect()->back()->with('success','Successfully '.$save->ro_status);
        // }
    }

    

	public function reject_status(Request $request){
        // return $request->all();
		$validator = Validator::make($request->all(), [
                    
            'reason' => 'required',
            'issues' => 'required',
            'remarks' => 'required'
        ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }

            
        $division_id = Auth::user()->division_id;
		$remarks = $request->input('remarks');
		$reason = $request->input('reason');
		$issues = $request->input('issues');
		$issue=implode(',', $issues);
		
		$dfo_status = $request->input('dfo_status');
		$ro_status = $request->input('ro_status');
		// print_r($request->all()); die();
		
		$applicant_permit_id = $request->session()->get('permit_id');
		$applicant_id = $request->session()->get('applicant_id');
		// dd($applicant_permit_id); die();
		if(applicationStatus::where('applicant_permit_id',$applicant_permit_id)->first()){
         // echo 'yes'; die();
			$save_reason = applicationStatus::where('applicant_permit_id',$applicant_permit_id)->first();
			// dd($save_reason); die();
			$old_range_id=$save_reason->range_id;
			$role_by=Auth::user()->role;
			if($role_by == 'ro')
			{
				$save_reason->applicant_id = $applicant_id;
				$save_reason->division_id = $division_id;
				$save_reason->applicant_permit_id = $applicant_permit_id;
				$save_reason->dfo_status = $dfo_status;
				$save_reason->ro_status = $ro_status;
				$save_reason->remarks = $remarks;
				$save_reason->reason = $reason;
				$save_reason->issue = $issue;
				$save_reason->user_status = 'Rejected by RO';
				// $save_reason->ip_action_dfo = $request->ip();
				$save_reason->role_by = Auth::user()->role;
				$save_reason->save();
			}elseif($role_by == 'dfo')
			{
				$save_reason->applicant_id = $applicant_id;
				$save_reason->division_id = $division_id;
				$save_reason->range_id = $old_range_id;
				$save_reason->applicant_permit_id = $applicant_permit_id;
				$save_reason->dfo_status = $dfo_status;
				$save_reason->user_status = 'Rejected by DFO';
				$save_reason->remarks = $remarks;
				$save_reason->reason = $reason;
				$save_reason->issue = $issue;
				$save_reason->role_by = Auth::user()->role;
				$save_reason->save();
			}
			$permit_status = PermitForTree::find($applicant_permit_id);
			$permit_status->dfo_status = $save_reason->dfo_status;
			$permit_status->save();
			return redirect()->back()->with('success','Successfully '.$dfo_status);
		}
		else{

			$save_reason = new applicationStatus();
			$save_reason->applicant_id = $applicant_id;
			$save_reason->division_id = $division_id;
			// $save_reason->range_id = $range_id;
			$save_reason->applicant_permit_id = $applicant_permit_id;
			$save_reason->dfo_status = $dfo_status;
			$save_reason->user_status = 'Rejected';
			$save_reason->remarks = $remarks;
			$save_reason->reason = $reason;
			$save_reason->issue = $issue;
			$save_reason->status_date = date('Y-m-d');
			// $save_reason->ip_action_dfo = $request->ip();
			$save_reason->role_by = Auth::user()->role;
			// $save_reason->by_id = Auth::user()->id;
			
			$save_reason->save();
			$permit_status = PermitForTree::find($applicant_permit_id);
			$permit_status->dfo_status = $save_reason->dfo_status;
			$permit_status->save();
			return redirect()->back()->with('success','Successfully '.$dfo_status);
		}

	
		
	}

	

	
	
	
	public function cfStatus(Request $request){

		$validator = Validator::make($request->all(), [
                    
            'cf_signature' => 'mimes:pdf|max:2048',

        ]);

            if($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }

		$applicant_permit_id = $request->session()->get('permit_id');
		$permit_no = $request->input('permit_no');
		$start_date = $request->input('start_date');
		$end_date = $request->input('end_date');
		$cf_signature = $request->file('cf_signature');
		if($request->hasFile('cf_signature') != ""){
        	$filename = $cf_signature->getClientOriginalName();
            $destinationPath = public_path('/cf_signature');
            $cf_signature->move($destinationPath, $filename);
            $cf_signature = '/cf_signature/' . $filename;
		}
		$dfo_status_accept = new Remarks();
		$dfo_status_accept->application_id = $applicant_permit_id;
		$dfo_status_accept->dfo_action = 'accepted';
		$dfo_status_accept->role_by = Auth::user()->role;
		$dfo_status_accept->by_id = Auth::user()->id;
		$dfo_status_accept->status = 'active';
		$dfo_status_accept->cf_signature = $cf_signature;
		$dfo_status_accept->permit_no = $permit_no;
		$dfo_status_accept->start_date = $start_date;
		$dfo_status_accept->end_date = $end_date;
		$dfo_status_accept->save();

		$permit_status = PermitForHunting::find($applicant_permit_id);
		$permit_status->dfo_status = $dfo_status_accept->id;
		$permit_status->save();

		return redirect()->back();
	}

	public function get_range_data(Request $request)
	{
		$range_id = $request->input('range_id');
		if($range_id == 0)
        {   
        	$data_list = PermitForHunting::with('getDivision','applicationStatus.getRange')->get();
        }else{
        	$data_list=PermitForHunting::join('application_status', 'application_status.applicant_permit_id', 'permit_for_huntings.id')
        	->join('forest_range', 'forest_range.id', 'application_status.range_id')
        	->where('application_status.range_id',$range_id)->get();
        	// $data_list = PermitForHunting::with('getDivision','applicationStatus.getRange')->where('division_id', $division_id)->get();
		}
		$table_list='';
		$count=1;
		if(!empty($data_list))
		{
			foreach ($data_list as $listing) 
	        {
	        	$table_list.='<tr>
	        	<th>'.$count.'</th>
	        	<td>'.$listing->date_of_issue.'</td>
	        	<td>'.$listing->applicant_name.'</td>';
	          $division=ForestDivision::where('id', $listing->division_id)->first();
	        	$table_list.='<td>'.$division->division_name.'</td>';
	        	if($listing->permit_type == 'crop_field')
	        	{
	        		$table_list.='<td>Crop Field</td>';
	        	}elseif($listing->permit_type == 'sarpunch')
	        	{
	        		$table_list.='<td>Sarpanch</td>';
	        	}else
	        	{
	        		$table_list.='<td>Air Force</td>';
	        	}
	        	$table_list.='<td>';
	        	if($listing->dfo_status)
	        	{
	        		if($listing->dfo_status == 'accepted')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-success" > Accepted</a>';
	        		}elseif($listing->dfo_status == 'rejected')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-danger" > Rejected</a>';
	        		}else
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-warning" > Pending</a>';
	        		}
	        	}else
	        	{
	        		$table_list.='<a class="btn btn-sm btn-light-warning" > Pending</a>';
	        	}
	        	$table_list.='</td><td>';
	        	if($listing->ro_status)
	        	{
	        		if($listing->ro_status == 'dfo')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-success" > Accepted</a>';
	        		}elseif($listing->ro_status == 'rejected')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-danger" > Rejected</a>';
	        		}
	        		else{
	        			$table_list.='<a class="btn btn-sm btn-light-warning" > Pending</a>';
	        		}
	        	}else{
	        		$table_list.='<a class="btn btn-sm btn-light-warning" > Pending</a>';
	        	}
	        	$table_list.='</td><td><a href="/admin/show/'.$listing->applicant_permit_id.'" class="btn btn-sm btn-light-info" > View </a></td>
	        	</tr>';
	        	$count++;
	        }
	    }else{
	    	$table_list.='<tr><td>No data found.</td></tr>';
	    	
	    }
		
		return array('data'=>$table_list);
	}

	

	public function get_data(Request $request)
	{
		$status=$request->input('status');
		$role_by=Auth::user()->role;
		if(Auth::user()->name == 'CF_North_Circle')
		{
			if($status == 'last_7days')
			{
				$date = \Carbon\Carbon::today()->subDays(7);
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')->where('application_status.user_status','Pending')
				->whereIn('permit_for_tree.division_id',['4','5'])
				 ->where('permit_for_tree.applied_date', '>=', $date)->get();
			}
			elseif($status == 'pending')
			{
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
				->whereIn('permit_for_tree.division_id',['4','5'])
	        	->where('application_status.user_status','Pending')->get();
			}
			else{
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
				->whereIn('permit_for_tree.division_id',['4','5'])->get();
			}
		}
		elseif(Auth::user()->name == 'CF_Shivalik')
		{
			if($status == 'last_7days')
			{
				$date = \Carbon\Carbon::today()->subDays(7);
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
				->where('application_status.user_status','Pending')
				 ->where('permit_for_tree.applied_date', '>=', $date)
				 ->whereIn('permit_for_tree.division_id',['1','2','3'])
				 ->get();
			}
			elseif($status == 'pending')
			{
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
	        	->where('application_status.user_status','Pending')
	        	->whereIn('permit_for_tree.division_id',['1','2','3'])
	        	->get();
			}
			else{

				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
				->whereIn('permit_for_tree.division_id',['1','2','3'])
				->get();
			}
		}
		elseif(Auth::user()->name == 'CCF_Hills_Zone')
		{
			if($status == 'last_7days')
			{
				$date = \Carbon\Carbon::today()->subDays(7);
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
				->where('application_status.user_status','pending')
				->where('permit_for_tree.applied_date', '>=', $date)
				->whereIn('permit_for_tree.division_id',['1','2','3','4','5'])
				->get();
			}
			elseif($status == 'pending')
			{
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
	        	->where('application_status.user_status','pending')
	        	->whereIn('permit_for_tree.division_id',['1','2','3','4','5'])
	        	->get();
			}
			else{
				$data_list=PermitForTree::join('application_status', 'application_status.applicant_permit_id', 'permit_for_tree.id')
				->whereIn('permit_for_tree.division_id',['1','2','3','4','5'])
				->get();
			}
		}
		
		$table_list='';
		$count=1;
		if(!empty($data_list))
		{
			foreach ($data_list as $listing) 
	        {
	        	$table_list.='<tr>
	        	<th>'.$count.'</th>
	        	<td>'.$listing->applied_date.'</td>
	        	<td>'.$listing->application_number.'</td>
	        	<td>'.$listing->applicant_name.'</td>';
	          $division=ForestDivision::where('id', $listing->division_id)->first();
	          // $range=ForestRange::where('id', $listing->range_id)->first();
	        	$table_list.='<td>'.$division->division_name.'</td>';
	        	
	        	$table_list.='<td>';
	        	if($listing->dfo_status)
	        	{
	        		if($listing->dfo_status == 'Accepted')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-success" > Accepted </a>';
	        		}elseif($listing->dfo_status == 'Rejected')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-danger" > Rejected by DFO </a>';
	        		}elseif($listing->dfo_status == 'Assign to CF')
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-success" > Assign to CF </a>';
	        		}
	        		else
	        		{
	        			$table_list.='<a class="btn btn-sm btn-light-warning" > Pending </a>';
	        		}
	        	}else
	        	{
	        		$table_list.='<a class="btn btn-sm btn-light-warning" > Pending</a>';
	        	}
	        	$table_list.='</td>';
	        	
	        	$table_list.='<td><a href="/admin/show/'.$listing->applicant_permit_id.'" class="btn btn-sm btn-light-info" > View </a></td>
	        	</tr>';
	        	$count++;
	        }
	    }else{
	    	$table_list.='<tr><td>No data found.</td></tr>';
	    }
		
		return array('data'=>$table_list);
	}

	
// end class 
}