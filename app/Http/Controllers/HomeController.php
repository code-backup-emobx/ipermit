<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\AllUsers;
use App\Models\PermitForTree;
use App\Models\TehsilDistrictDivision;
use App\Models\OwnerDetails;


class HomeController extends Controller
{

   
	public function index(Request $request){
		$phone_number_applicant = $request->session()->get('phone_number');
        $id_applicant = $request->session()->get('id');
		
		$tehsils = TehsilDistrictDivision::whereIn('division_id',['1','2','3','4','5'])->where('status', 'Active')->orderBy('tehsil')->get();
	  return view('/permit_for_tree',compact('tehsils'));
	}

	public function user_applications(Request $request)
    {
        $data=array();
        $phone_number_applicant = $request->session()->get('phone_number');
        $id_applicant = $request->session()->get('id');
        // print(date('Y-m-d')); die();
        $user_applications = PermitForTree::with('getDivision','getTehsil','applicationStatus')
        ->where('applicant_id',$id_applicant)->get();
        
        $data['user_applications']=$user_applications;
        return view('/user_list',$data);
    }

	
	// session value
	  public function GetAddress(Request $request){
      $error_code='500';

      $tehsil=$request->tehsil;
      $get_address=TehsilDistrictDivision::where([
        'id'=>$tehsil,
        'status'=>'Active'
      ])->with('getDivision');

      if($get_address->count() > 0){
 
        $error_code='200';
        $get_address=$get_address->first();
      }
    return array('error_code'=>$error_code,'data'=>$get_address);
    }
	
	public function submitPermittree(Request $request){
	
    			$validator = Validator::make($request->all(), [
                    
            'profile_pic' => 'required|mimes:jpeg,jpg|max:200',
            'residence_address' => 'required',
            'applicant_name' => 'required',
            'aadhar_number' => 'required',
            'village_name' => 'required',
            'tehsil'  => 'required',
            'district' => 'required',
            'division_id'     => 'required',
            'khasra_number'     => 'required',
            // 'measle_details' => 'required',
            'no_tree_cut' => 'required',
            // 'name_of_owner' => 'required',
            'fard_pdf' => 'required|max:2048',
            'aadhar_copy' => 'required|mimes:jpeg,jpg|max:200'
        ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }		
    
			// return $request->all();die();
		$id_applicant = $request->session()->get('id');
		$applied_date = date('Y-m-d');
		$section_act = $request->input('section_act');	
		$applicant_name = $request->input('applicant_name');	
		$aadhar_number = $request->input('aadhar_number');	
        $residence_address = $request->input('residence_address');  
		$village_name = $request->input('village_name');	
		$tehsil = $request->input('tehsil');
		$district = $request->input('district');
		$division_id = $request->input('division_id');
		$measle_details = $request->input('measle_details');	
		$no_tree_cut = $request->input('no_tree_cut');	
		$profile_pic = $request->hasfile('profile_pic');
		$name_of_owner = $request->input('name_of_owner');

		$owner_name = $request->input('owner_name');
		$owner_father_name = $request->input('owner_father_name');

		$owner_signature=array();
        if($request->hasFile('owner_signature') != "")
        {
            foreach($request->file('owner_signature') as $key=>$file)
            {
                $name=$file->getClientOriginalName();    
                $file->move(public_path().'/media/', $name);      
                $owner_signature[$key] = '/media/'.$name;  
            }
        }



		if($request->hasFile('profile_pic') != "") {
            $profile_pic = $request->file('profile_pic');
            // $filename = time() . '.' . $profile_pic->getClientOriginalExtension();
            $filename =$profile_pic->getClientOriginalName();
            $destinationPath = public_path('/media');
            $profile_pic->move($destinationPath, $filename);
            $profile_pic = '/media/' . $filename;
		}
        
        $fard_pdf='';
		if($request->hasFile('fard_pdf') != ""){
			$fard_pdf = $request->file('fard_pdf');
        	$filename =$fard_pdf->getClientOriginalName();
            $destinationPath = public_path('/fard_pdf');
            $fard_pdf->move($destinationPath, $filename);
            $fard_pdf = '/fard_pdf/' . $filename;
		}

		$aadhar_card='';
		if($request->hasFile('aadhar_copy') != ""){
			$aadhar_card = $request->file('aadhar_copy');
        	$filename =$aadhar_card->getClientOriginalName();
            $destinationPath = public_path('/aadhar_card');
            $aadhar_card->move($destinationPath, $filename);
            $aadhar_card = '/aadhar_card/' . $filename;
		}

		$poa='';
		if($request->hasFile('poa') != ""){
			$poa = $request->file('poa');
        	$filename =$poa->getClientOriginalName();
            $destinationPath = public_path('/media');
            $poa->move($destinationPath, $filename);
            $poa = '/media/' . $filename;
		}
// return $poa;
		$save_detail = new PermitForTree();
		// $save_detail->section_act = $section_act;
		$save_detail->profile_pic = $profile_pic;
		$save_detail->applicant_id = $id_applicant;
		$save_detail->applicant_name = $applicant_name;
		$save_detail->aadhar_number = $aadhar_number;
		$save_detail->aadhar_card = $aadhar_card;
        $save_detail->residence_address = $residence_address;
		$save_detail->village_name = $village_name;
		$save_detail->tehsil = $tehsil;
		$save_detail->district = $district;
		$save_detail->division_id = $division_id;
		$save_detail->fard_pdf = $fard_pdf;
		$save_detail->measle_details = $measle_details;
		$save_detail->no_tree_cut = $no_tree_cut;
		$save_detail->name_of_owner = $name_of_owner;
		$save_detail->applied_date = $applied_date;
		$save_detail->poa = $poa;
        $save_detail->user_status = 'Pending';
        $save_detail->dfo_status = 'Assign to DFO';
        $save_detail->ro_status = 'Pending';
        $save_detail->cf_status = 'Pending';
		$save_detail->save();
        
         
        $application_number=10000000+$save_detail->id;
        $applicant=PermitForTree::where('id', $save_detail->id)->first();
        $applicant->application_number=$application_number;
        $applicant->save();

        for($i=0;$i<count($owner_name);$i++)
        {
        	$user_owner = new OwnerDetails();
        	$user_owner->applicant_permit_id=$save_detail->id;
        	$user_owner->applicant_id=$id_applicant;
        	$user_owner->owner_name=$owner_name[$i];
        	$user_owner->owner_father_name=$owner_father_name[$i];
        	if($owner_signature[$i]){$user_owner->owner_sign=$owner_signature[$i];}
        	$user_owner->save();
        }


        $templateId=2889;
        $mobile_number = $request->session()->get('phone_number');
        sendSingleSMS($mobile_number,$templateId, $application_number);
		return back()->with('success','Your Application '.$application_number.' Submit Successfully');

	}


	public function get_more_ownerinfo(Request $request)
    {
        $n=$request->input('n');
        $n2=$n-1;
		$html='';
		$html.='<div class="card col-xl-12 pt-5 pb-5 mb-5 owners_'.$n.'"><div class=""><button type="button" class="close" onclick="close_owner('.$n.');" aria-label="Close"><i aria-hidden="true" class="ki ki-close"></i></button></div><div class="form-group"><label><b>Owner Name</b><font color="red"> * </font></label><input type="text" class="form-control  form-control-lg" name="owner_name[]" id="owner_name" value="" /></div><div class="form-group"><label><b>Owner Father Name</b><font color="red"> * </font></label><input type="text" class="form-control  form-control-lg" name="owner_father_name[]" id="owner_father_name" value="" /></div><div class="form-group"><label><b>Upload Owner Signautre </b><font color="red"> * </font></label><br><label for="owner_signature_'.$n.'"><img src="'.url('assets/media/add.png').'" style="width: 100px; height:100px;" alt="" id="add_sign_'.$n.'"></label><a id="cross_sign_'.$n.'" onclick="remove_pic(add_sign_'.$n.', cross_sign_'.$n.');" style="display: none; position: absolute;"><i class="far fa-times-circle"></i></a><input type="file" name="owner_signature['.$n2.']" id="owner_signature_'.$n.'" class="inputfile inputfile-5" onchange="signURL(this, '.$n.');" accept="image/jpeg" required /></div></div>';
        return $html;
	}
// end class 
}

