<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ApplicantLogin extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'applicant_login';
}