<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AnswerTables extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'answer_tables';
}