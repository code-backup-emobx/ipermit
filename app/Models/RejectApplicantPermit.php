<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RejectApplicantPermit extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'remarks';
}