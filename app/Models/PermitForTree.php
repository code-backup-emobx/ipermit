<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PermitForTree extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'permit_for_tree';

    public function getDivision(){

       return $this->hasOne('App\Models\ForestDivision','id','division_id');
    }

	public function getTehsil(){

	    return $this->hasOne('App\Models\TehsilDistrictDivision','tehsil');
	}

	
	public function applicationStatus(){

		return $this->hasOne('App\Models\applicationStatus','applicant_permit_id');
	}

	
	public function UserHuntingaddress(){

		return $this->hasMany('App\Models\HuntingAddress','applicant_permit_id');
	}
}