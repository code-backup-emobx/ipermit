<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OwnerDetails extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'owner_details';
}