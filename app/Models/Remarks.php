<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Remarks extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'remarks';
}