<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class QuestionTables extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'question_tables';
}