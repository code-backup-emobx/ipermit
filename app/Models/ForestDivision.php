<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestDivision extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_divisions';
}