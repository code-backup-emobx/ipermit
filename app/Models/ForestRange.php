<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestRange extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_range';
}