<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class applicationStatus extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'application_status';

    public function getRange(){

       return $this->hasOne('App\Models\ForestRange','id','range_id');
    }
}