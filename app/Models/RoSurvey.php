<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RoSurvey extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'ro_survey';
}