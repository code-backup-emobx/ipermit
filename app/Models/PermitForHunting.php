<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PermitForHunting extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'permit_for_huntings';

    public function getDivision(){

       return $this->hasOne('App\Models\ForestDivision','id','division_id');
    }

	public function getTehsil(){

	    return $this->hasOne('App\Models\TehsilDistrictDivision','id','tehsil');
	}

	public function getRemarks(){

		return $this->belongsTo('App\Models\Remarks','dfo_status');
	}
}