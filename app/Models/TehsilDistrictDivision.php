<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TehsilDistrictDivision extends Model
{
    public function getDivision(){

       return $this->hasOne('App\Models\ForestDivision','id','division_id');
    }
}
