<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();

Route::get('/', function () {
   if(auth()->check()){
  if(auth()->user()->role == 'dfo'){
 return redirect()->intended('adminLogin/dashboard');
  }
  if(auth()->user()->role == 'cfw'){
 return redirect()->intended('adminLogin/home');
  }
  if(auth()->user()->role == 'ro'){
  return redirect()->intended('adminLogin/rohome');
  }
  
   
   
   }else{
    return redirect(route('login'));
   }
});



Route::get('login',[\App\Http\Controllers\Auth\ApplicantLoginController::class,'showlogin'])->name('login');
Route::post('login',[\App\Http\Controllers\Auth\ApplicantLoginController::class,'login'])->name('login');
Route::post('admin/logout',[\App\Http\Controllers\AdminLoginController::class,'logout'])->name('admin.logout');

Route::get('otp',[\App\Http\Controllers\Auth\ApplicantLoginController::class,'otp'])->name('otp');
Route::post('otp',[\App\Http\Controllers\Auth\ApplicantLoginController::class,'otp_login'])->name('otp');

        
Route::get('permit_for_tree',[\App\Http\Controllers\HomeController::class,'index'])->name('permit_for_tree');
Route::post('permit_for_tree',[\App\Http\Controllers\HomeController::class,'submitPermittree'])->name('permit_for_tree');

Route::get('application_details/{id}',[\App\Http\Controllers\HomeController::class,'application_details']);

Route::post('application_details/{id}', [App\Http\Controllers\HomeController::class,'update_user_application'])->name('application_details.update');

Route::get('user_noc/{id}',[\App\Http\Controllers\HomeController::class,'user_noc'])->name('user_noc');
Route::get('nocpdf/{id}',[\App\Http\Controllers\HomeController::class,'noc']);

//Route::get('nocpdf',array('as'=>'nocpdf','uses'=>'HomeController@noc'));

Route::get('user_list',[\App\Http\Controllers\HomeController::class,'user_applications'])->name('user_list');

Route::post('getaddress',[\App\Http\Controllers\HomeController::class,'GetAddress'])->name('getaddress');
Route::post('get_more_owners',[\App\Http\Controllers\HomeController::class,'get_more_ownerinfo'])->name('get_more_owners');

Route::post('get_single_weaponinfo',[\App\Http\Controllers\HomeController::class,'get_single_weaponinfo'])->name('get_single_weaponinfo');

 Route::post('get_more_address',[\App\Http\Controllers\HomeController::class,'get_more_address'])->name('get_more_address');

 Route::post('get_more_hunters',[\App\Http\Controllers\HomeController::class,'get_more_hunters'])->name('get_more_hunters');
 



Route::get('admin/login',[\App\Http\Controllers\AdminLoginController::class,'admin_login'])->name('admin.login');
Route::post('admin/login',[\App\Http\Controllers\AdminLoginController::class,'adminLogin']);

Route::post('get_range',[\App\Http\Controllers\AdminLoginController::class,'get_range'])->name('get_range');


Route::get('adminLogin/dashboard',[\App\Http\Controllers\AdminDashboardController::class,'adminDashboard']);
Route::get('adminLogin/cf',[\App\Http\Controllers\AdminDashboardController::class,'cf_home']);
Route::get('adminLogin/rohome',[\App\Http\Controllers\AdminDashboardController::class,'ro_dashboard']);
Route::get('adminLogin/home',[\App\Http\Controllers\AdminDashboardController::class,'admin']);

Route::get('adminLogin/cfshiwalik',[\App\Http\Controllers\AdminDashboardController::class,'cf_shiwalik_home']);

Route::get('adminLogin/cfppa',[\App\Http\Controllers\AdminDashboardController::class,'cf_ppa_home']);


Route::get('admin/view_applicant_detail/{id}',[\App\Http\Controllers\AdminDashboardController::class,'viewApplicantDetail']);
Route::get('admin/ro/view_applicant_detail/{id}',[\App\Http\Controllers\AdminDashboardController::class,'viewApplicantDetailby_Ro']);

Route::post('/admin/get_range',[\App\Http\Controllers\AdminDashboardController::class,'get_range']);
Route::post('/admin/get_range_data',[\App\Http\Controllers\AdminDashboardController::class,'get_range_data']);

Route::post('/admin/get_data',[\App\Http\Controllers\AdminDashboardController::class,'get_data']);

Route::post('/admin/status',[\App\Http\Controllers\AdminDashboardController::class,'application_status']);
Route::post('/admin/dfo_survey',[\App\Http\Controllers\AdminDashboardController::class,'dfo_survey']);
Route::post('/admin/ro_survey',[\App\Http\Controllers\AdminDashboardController::class,'ro_survey']);
Route::post('/admin/cf_survey',[\App\Http\Controllers\AdminDashboardController::class,'cf_survey']);

Route::post('/admin/check_survey',[\App\Http\Controllers\AdminDashboardController::class,'check_survey']);
Route::post('/admin/reject_status',[\App\Http\Controllers\AdminDashboardController::class,'reject_status']);

Route::get('/admin/dfo_status/{id}',[\App\Http\Controllers\AdminDashboardController::class,'dfoStatus']);
Route::get('adminLogin/cf',[\App\Http\Controllers\AdminDashboardController::class,'cf_home']);

Route::get('/admin/show/{id}',[\App\Http\Controllers\AdminDashboardController::class,'show']);
Route::post('/admin/cfstatus',[\App\Http\Controllers\AdminDashboardController::class,'cfStatusApplicantPermit']);
Route::post('/admin/cf_status',[\App\Http\Controllers\AdminDashboardController::class,'cfStatus']);
