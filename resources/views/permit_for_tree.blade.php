@extends('layouts.app1')

@section('content')

  <!-- <div class="d-flex flex-column-fluid"> -->
        <!--begin::Container-->

    <div class=" container ">
            <div class="card card-custom">
    <div class="card-body p-0">
        <!--begin: Wizard-->
        <div class="wizard wizard-2" id="kt_wizard_v2" data-wizard-state="step-first" data-wizard-clickable="false">
            <!--begin: Wizard Nav-->
            <div class="wizard-nav border-right py-8 px-8 py-lg-20 px-lg-10" style="background-color: #297430">
                <!--begin::Wizard Step 1 Nav-->
             
                <div class="wizard-steps" >

                    <!--end::Wizard Step 1 Nav-->
                    <div class="wizard-step" data-wizard-type="step" data-wizard-state=""style="background-color: #048404">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							        <polygon points="0 0 24 0 24 24 0 24"/>
							        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
							        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
							    </g>
								</svg><!--end::Svg Icon--></span>                            
							</div>
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                     <a href="{{url('/permit_for_tree')}}" style="color:white;">Permit For Tree</a>
                                </h3>
                                <!-- <div class="wizard-desc text-white">
                                  
                                </div> -->
                            </div>

                        </div>
                    </div>
                    <div style="padding:2%;"></div>
                    <!-- <div style="padding:2%;"></div> -->
                    <!-- Wizard second start -->
                     <div class="wizard-step" data-wizard-type="step" data-wizard-state=""style="background-color: #048404">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                                </g>
                                </svg> 
                                <!--end::Svg Icon-->
                            </span>                            
                          </div>
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                     <a href="{{url('/user_list')}}" style="color:white;">Previous Applications</a>
                                     <!-- <a href="" style="color:white;">Previous Applications</a> -->
                                </h3>
                                <div class="wizard-desc text-white">
                                  
                                </div>
                            </div>

                        </div>
                    </div>  
                    <!-- Wizard second ends -->
                    <!--end::Wizard Step 1 Nav-->

                   

                    <!--begin::Wizard Step 2 Nav-->
                    <!-- <div class="wizard-step" data-wizard-type="step">
                        <div class="wizard-wrapper">
                            <div class="wizard-icon">
                                <span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Thunder-move.svg--><!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							        <rect x="0" y="0" width="24" height="24"/>
							        <path d="M16.3740377,19.9389434 L22.2226499,11.1660251 C22.4524142,10.8213786 22.3592838,10.3557266 22.0146373,10.1259623 C21.8914367,10.0438285 21.7466809,10 21.5986122,10 L17,10 L17,4.47708173 C17,4.06286817 16.6642136,3.72708173 16.25,3.72708173 C15.9992351,3.72708173 15.7650616,3.85240758 15.6259623,4.06105658 L9.7773501,12.8339749 C9.54758575,13.1786214 9.64071616,13.6442734 9.98536267,13.8740377 C10.1085633,13.9561715 10.2533191,14 10.4013878,14 L15,14 L15,19.5229183 C15,19.9371318 15.3357864,20.2729183 15.75,20.2729183 C16.0007649,20.2729183 16.2349384,20.1475924 16.3740377,19.9389434 Z" fill="#000000"/>
							        <path d="M4.5,5 L9.5,5 C10.3284271,5 11,5.67157288 11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L4.5,8 C3.67157288,8 3,7.32842712 3,6.5 C3,5.67157288 3.67157288,5 4.5,5 Z M4.5,17 L9.5,17 C10.3284271,17 11,17.6715729 11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L4.5,20 C3.67157288,20 3,19.3284271 3,18.5 C3,17.6715729 3.67157288,17 4.5,17 Z M2.5,11 L6.5,11 C7.32842712,11 8,11.6715729 8,12.5 C8,13.3284271 7.32842712,14 6.5,14 L2.5,14 C1.67157288,14 1,13.3284271 1,12.5 C1,11.6715729 1.67157288,11 2.5,11 Z" fill="#000000" opacity="0.3"/>
							    </g>
								</svg><!--><!-- end::Svg Icon</span>  
							</div>  
                            <div class="wizard-label">
                                <h3 class="wizard-title text-white">
                                     Upload Ownership Documents
                                </h3>
                                <div class="wizard-desc text-white">
                                     Upload Your Ownership Documents
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!--end::Wizard Step 2 Nav-->
                </div>
            </div>
            <!--end: Wizard Nav-->

            <!--begin: Wizard Body-->
            <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                <!--begin: Wizard Form-->
                <div class="row">
                    <div class="offset-xxl-2 col-xxl-8">
                        <form class="form" id="kt_form" action="{{url('/permit_for_tree')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <!--begin: Wizard Step 1-->
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <h4 class="mb-10" style='color:green;text-align: center'>PERMIT FOR TREE</h4>
                                <h4 class="mb-5" style='color:green'>Application for obtaining Felling Permit of trees from Private areas closed under Section 4
and 5 of The Punjab Land Preservation Act,1900</h4>
                                 <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group ">
                                            <label><b> Permit for </b><font color="red"> * </font></label>
                                            <div class="radio-inline ">
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Individual
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" 
                                                    value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Joint Ownership
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" 
                                                    value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Mushtarka
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" 
                                                    value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Panchayat
                                                </label>
                                            </div>
                                            <!-- <div class="radio-inline ">
                                                <label class="radio">
                                                    <input type="radio" name="permit_type" value="private" class="permit_type" checked/>
                                                    <span></span>
                                                    Private
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="permit_type" value="panchayat" class="permit_type" />
                                                    <span></span>
                                                   Panchayat
                                                </label>
                                             
                                            </div> -->
                                            <!-- <span class="form-text text-muted">Please Choose Permit for.</span> -->
                                        </div>
                                        <!-- <div class="form-group private_type">
                                            <div class="radio-inline ">
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Individual
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" 
                                                    value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Joint Ownership
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" 
                                                    value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Mushtarka
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="crop_permit_type" 
                                                    value="individual" class="crop_permit_type" />
                                                    <span></span>
                                                    Panchayat
                                                </label>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-3">
                                        <label><b> Applicant’s Photo </b><font color="red"> * </font></label>
                                        <label for="replace-profile">
                                        <img src="{{asset('assets/media/add.png')}}" style="width: 100px; height:100px;" id="replace_img">

                                        </label>
                                        <a onclick="remove_pic('replace_img', 'cross_profile')" id="cross_profile" style="display: none; position: absolute;"><i class="far fa-times-circle"></i></a>
                                        <div class="form-group profile_pic">
                                            <!-- <input type="file" required name="profile_pic" id="replace-profile" class="inputfile inputfile-5" onchange="readURL(this);" accept="image/jpeg"> -->
                                            <input type='file' required name="profile_pic" id="replace-profile" class="inputfile inputfile-5" onchange="readURL(this);" accept="image/jpeg">
                                           <!--  <span id='val'></span>
                                            <span id='button'>Select File</span> -->
                                        </div>
                                    </div>
                                </div>

                                <!--begin::Input-->
                                <!-- <div class="form-group">
                                    <label><b>Area Closed Under Section 4/5 of the Act 1900</b><font color="red"> * </font></label>
                                    <input type="text" required  class="form-control form-control-lg" 
                                    name="section_act" id="section_act" placeholder="" value="">
                                    
                                   
                                </div> -->

                                <div class="form-group">
                                    <label><b>Applicant Name</b><font color="red"> * </font></label>
                                    <input type="text" required  class="form-control form-control-lg" 
                                    name="applicant_name" placeholder="" value="">
                                    
                                    <!-- <span class="form-text text-muted">Please enter applicant name.</span> -->
                                </div>

                                <div class="form-group">
                                    <label><b>Aadhar Card Number</b><font color="red"> * </font></label>
                                    <input type="text" required  class="form-control form-control-lg" name="aadhar_number" id="aadhar_number" placeholder="" value="" maxlength="12">
                                    
                                    <!-- <span class="form-text text-muted error">Please enter aadhar card number.</span> -->
                                </div>

                                <div class="form-group">
                                    <label><b>Aadhar Card copy</b><font color="red"> * </font></label><br>
                                    
                                    <label for="add_aadhar">
                                        <img src="{{asset('assets/media/add.png')}}" style="width: 100px; height:100px;" alt="" id="add_img">
                                    </label>
                                    <a onclick="remove_pic('add_img', 'cross_aadhar')" id="cross_aadhar" style="display: none; position: absolute;"><i class="far fa-times-circle"></i></a>
                                    <input type="file" name="aadhar_copy" id="add_aadhar" class="inputfile inputfile-5" onchange="aadharURL(this);" accept="image/jpeg" required />
                                    <!-- <input type="file" name="aadhar_copy" accept="image/jpeg" required class="form-control form-control-lg" /> -->
                                    <!-- <a class="btn btn-sm font-weight-bold ">Choose File</a> -->

                                    <!-- <input type='file' name="aadhar_copy" accept="image/jpeg" id="aa" onchange="pressed()"><button id="fileLabel">Choose File</button> -->
                                     
                                    <!-- <span class="form-text text-muted">Please upload scan copy of Aadhar Card.</span> -->
                                </div>

                                <div class="form-group">
                                    <label><b>Residence Address </b><font color="red"> * </font></label>
                                    <input type="text" required  class="form-control form-control-lg" name="residence_address" id="address_of_applicant" placeholder="" value="">
                                    
                                    <!-- <span class="form-text text-muted">Please enter residence address.</span> -->
                                </div>


                                <div class="row add_address_info">
                                    <h4 class="mb-5" style='color:green;text-align: center'>Area Detail (where felling to be done)</h4>
                                    <div class="card col-xl-12 pt-5 pb-5 mb-5">
                                       
                                        <div class="form-group" id="show_village_name">
                                            <label><b>Village Name </b><font color="red"> * </font></label>
                                            <input type="text" required class="form-control form-control-lg" name="village_name" id="village_name_crop_field" placeholder="" value=""/>
                                            <!-- <span class="form-text text-muted">Please enter your village name.</span> -->
                                        </div>
                                    
                                        <!--begin::Input-->
                                        <div class="form-group">
                                            <label><b>Tehsil </b><font color="red"> * </font></label>
                                            <select class="form-control  form-control-lg" 
                                            name="tehsil" id="tehsil" onchange="getaddress(this.value, 1)">
                                                <option>Seletct Tehsil</option>
                                                @if(!empty($tehsils))
                                                @foreach($tehsils as $tehsil)
                                                <option value="{{$tehsil->id}} @if(old('tehsil') == $tehsil->id) @endif">{{$tehsil->tehsil}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <!-- <span class="form-text text-muted">Please enter tehsil name.</span> -->
                                        </div>
                                  
                                        <div class="form-group">
                                            <label><b>District </b><font color="red"> * </font></label>
                                            <input type="text" required readon class="form-control  form-control-lg district1" name="district" id="district" readonly  value=""  required />
                                            <!-- <span class="form-text text-muted">District.</span> -->
                                        </div>
                                   
                                        <div class="form-group">
                                            <label><b>Division </b><font color="red"> * </font></label>
                                            <input type="text" required class="form-control  form-control-lg division1"  id="division" readonly value="" required />

                                        <input type="hidden" class="form-control form-control-lg division_id1" name="division_id" id="division_id" readonly value="" required />
                                            <!-- <span class="form-text text-muted">Division.</span> -->
                                        </div>

                                        
                                        <div class="form-group">
                                            <label><b>Pdf Of Fard </b><font color="red"> * </font></label>
                                            <br>
                                            <label for="fard_pdf" id="iframe_pdf">
                                                <img src="{{asset('assets/media/add.png')}}" style="width: 100px; height:100px;" alt="" id="add_fard">
                                            </label>
                                            <a onclick="remove_pic('add_fard', 'cross_fard')" id="cross_fard" style="display: none; position: absolute;"><i class="far fa-times-circle"></i></a>
                                            <input type="file" id="fard_pdf"  name="fard_pdf" accept="application/pdf" class="inputfile inputfile-5" onchange="fardURL(this);" required />
                                        </div> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b>Khasra Number</b><font color="red"> * </font></label>
                                    <input type="text" required class="form-control form-control-lg" name="khasra_number" value="">
                                    <!-- <span class="form-text text-muted">Please enter khasra number.</span> -->
                                </div>

                                <!-- <div class="form-group" id="show_khasra_number">
                                    <label><b>Khasra number of Rakba details</b><font color="red"> * </font></label>
                                    <input type="text" required class="form-control form-control-lg" name="measle_details"  id="khasra_number" value="">
                                </div> -->
                           
                                <div class="form-group">
                                    <label><b>Approximate number of trees to be cut</b><font color="red"> * </font></label>
                                    <input type="text" required class="form-control form-control-lg" name="no_tree_cut"  id="khasra_number" value="">
                                    <!-- <span class="form-text text-muted">Please Enter number of trees to be cut.</span> -->
                                </div>

                                <!-- <div class="form-group" id="show_khasra_number">
                                    <label><b>Owner and Owner Details</b><font color="red"> * </font></label>
                                    <input type="text" required class="form-control form-control-lg" name="name_of_owner"  id="khasra_number" value="">
                                    
                                </div>
 -->

                                <div class="row add_owner_info">
                                        <h4 class="mb-5" style='color:green;text-align: center'>Owner Details</h4>
                                    
                                        <div class="card col-xl-12 pt-5 pb-5 mb-5 ">
                                            
                                            
                                            <div class="form-group">
                                                <label><b>Owner Name</b><font color="red"> * </font></label>
                                                <input type="text" class="form-control  form-control-lg" name="owner_name[]" id="owner_name" placeholder="" value="" />
                                                <!-- <span class="form-text text-muted">Please Enter Owner Name</span> -->
                                            </div>
                                     
                                            <div class="form-group">
                                                <label><b>Owner Father Name </b><font color="red"> * </font></label>
                                                <input type="text" class="form-control  form-control-lg" name="owner_father_name[]" id=" owner_father_name" placeholder="" value="" />
                                                <!-- <span class="form-text text-muted">Please Enter Owner Father Name</span> -->
                                            </div>

                                            <div class="form-group">
                                                <label><b>Upload Owner Signautre </b><font color="red"> * </font></label>
                                                <br>
                                    
                                    <label for="owner_signature_1">
                                        <img src="{{asset('assets/media/add.png')}}" style="width: 100px; height:100px;" alt="" id="add_sign_1">
                                    </label>
                                    <a id="cross_sign_1" onclick="remove_pic('add_sign_1', 'cross_sign_1')" style="display: none; position: absolute;"><i class="far fa-times-circle"></i></a>
                                    <input type="file" name="owner_signature[0]" id="owner_signature_1" class="inputfile inputfile-5" onchange="signURL(this, 1);" accept="image/jpeg" required />

                                                <!-- <input type="file" class="form-control  form-control-lg owner_signature" name="owner_signature[]" id=" owner_signature" placeholder="" value="" accept="image/jpeg"/>
                                                <a class="btn btn-sm font-weight-bold " onclick="file_choose('owner_signature');">Choose File</a> -->
                                                <!-- <span class="form-text text-muted">Please Upload Signature of Owner</span> -->
                                            </div>
                                               
                                        </div>
                                        
                                    </div>
                                    <a class="btn btn-sm font-weight-bold text-uppercase mb-5 add_owner" style="background-color:#297430;color:white;font-size: 14px;" value="2">Add</a>


                                <div class="form-group">
                                    <label><b>Affidavit/Power of Attorney </b><font color="red"> * </font></label><br>
                                    <label for="poa_pdf">
                                        <img src="{{asset('assets/media/add.png')}}" style="width: 100px; height:100px;" alt="" id="add_poa">

                                        
                                    </label>
                                    <a id="cross_poa" onclick="remove_pic('add_poa','cross_poa')" style="display: none; position: absolute;"><i class="far fa-times-circle"></i></a>
                                    <input type="file" id="poa_pdf"  name="poa" accept="application/pdf" class="inputfile inputfile-5" onchange="poaURL(this);" required />
                                    <!-- <input type="file" class="form-control form-control-lg poa" name="poa" accept="application/pdf" required/>
                                    <a class="btn btn-sm font-weight-bold " onclick="file_choose('poa');">Choose File</a> -->
                                    <!-- <span class="form-text text-muted">Please upload the Pdf of Fard.</span> -->
                                </div> 
                                
                                <div class="form-group ">
                                  <div class="checkbox-inline">
                                    <label class="checkbox">
                                        <input type="checkbox" name="verify_terms" value="yes"/>
                                        <span></span>
                                        By ticking the checkbox, I confirm that all the details mentioned in above application form are correct and I shall be responsible for any legal obligation/action pertaining to wrong information mentioned.
                                    </label>
                                  </div>
                                </div>
                     

                                <!--end::Input-->
                                <div class="form-group">
                                <button type="submit" class="btn btn-sm font-weight-bold text-uppercase px-9 py-4" style="background-color:#297430;color:white;font-size: initial;" >Submit</button>
                                </div>

                                

                                   <!--end::Input-->
                        </div>
                            <!--end: Wizard Step 1-->
                          
                            <!--begin: Wizard Actions-->
                           
                            <!--end: Wizard Actions-->
                        </form>
                    </div>
                    <!--end: Wizard-->
                </div>
                <!--end: Wizard Form-->
            </div>
            <!--end: Wizard Body-->
        </div>
        <!--end: Wizard-->
        <!-- </div> -->
        <!--end: Wizard-->
    </div>
</div>
</div>
        <!--end::Container-->
<!--     </div> -->

<div class="modal fade" id="show_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title_3">You need following documents and details before filling forms. </h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button> -->

            </div>
            <div class="modal-body">
             
              <p>1. Aadhar Card </p>
              <p>2. Weapon License Pdf </p>
              <p>3. Fard Pdf of Hunting Address </p>
              <p>4. Designation Consent Pdf for Sarpanch only</p>
            </div>
            <div class="modal-footer">
            
                <button class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          
        </div>
    </div>
</div>

@endsection
@push('page-script')
<!-- <script type="text/javascript">
    $(document).ready(function () {
        $('#show_popup').modal('show');

        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='hunter']:checked").val();
            if(radioValue == 'no'){
                // alert("Your are a - " + radioValue);
                $('#multiple_hunters').show();
                $('.add_hunter').show();
                $('.name_of_hunter').hide();
                $('.single_weapon_info').hide();
            }else{
                var name= $('#name_of_applicant').val();
                // alert(name);
                $('#name_of_hunter').val(name);
                $('.name_of_hunter').show();
                $('.single_weapon_info').show();
                $('#multiple_hunters').hide();
                $('.add_hunter').hide();
            }
        });
    });
</script> -->
<script>

$(function () {
    $('#arm_validity_no').datepicker({
        dateFormat: 'yyyy/mm/dd',
        minDate: new Date(),
        inline: true
    });
});

function remove_pic(input, cross_input){
    $('#'+input).attr('src', 'assets/media/add.png');
    $('#'+cross_input).hide();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#replace_img').attr('src', e.target.result);
            $('#cross_profile').show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function aadharURL(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#add_img').attr('src', e.target.result);
            $('#cross_aadhar').show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function fardURL(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#add_fard').attr('src', 'assets/media/add_pdf.png');
            // $('#iframe_pdf').html('<iframe src="'+e.target.result+'" style="width: 100px; height:100px;" alt="" id="add_fard"></iframe>');
            $('#cross_fard').show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function poaURL(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#add_poa').attr('src', 'assets/media/add_pdf.png');
            $('#cross_poa').show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function signURL(input, n)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#add_sign_'+n).attr('src', e.target.result);
            $('#cross_sign_'+n).show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function hunter_readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#replace_hunter_img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function close_owner(n)
    {
        // alert('working');
        $('.owners_'+n).remove();
        // n=parseInt(n)-1;
        $('.add_owner').attr('value', n);
        $('.add_owner').show();

    }

    function file_choose(field)
    {
        if(field == 'fard_pdf')
        {
            $(".fard_pdf").trigger('click');
        }else if(field == 'owner_signature')
        {
            $(".owner_signature").trigger('click');
        }
        else if(field == 'poa')
        {
            $(".poa").trigger('click');
        }
    }

$(document).ready(function(){


    // $('.file_choose').click(function(){
    //    $(".aadhar_copy").trigger('click');
    // })

    // $("input[type='file']").change(function(){
    //    $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
    // }) 

    // $('.file-upload').on('click', function(e) {
    //   e.preventDefault();
    //   $('#file-input').trigger('click');
    // });   



    $(".permit_type").click(function(){
        
        var permit_value = $("input[name='permit_type']:checked").val();
        // alert(permit_value);
        if(permit_value == 'private')
        {
            $('.private_type').show();
            
        }else{
            
            $('.private_type').hide();
        }
    });

    $(".add_owner").click(function(){
        var n=$(this).attr('value');
        $.ajax({
          method: 'POST', // Type of response and matches what we said in the route
          headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
          data: {'n' : n},
          url: "{{route('get_more_owners')}}",
         // This is the url we gave in the route
          // data: {'division_id' : division_id}, // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response); 
            
            $('.add_owner_info').append(response);
          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
        });
        n=parseInt(n)+1;
        $(this).attr('value', n);
        // alert(n);
        // if(n == 3)
        // {
        //     $(this).hide();
        // }
    });
    
});
</script>
   
<script>
    
    $(document).ready(function () {
      //called when key is pressed in textbox
        $("#aadhar_number").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            // $(".error").addClass("text-danger");
            $(".error").html("<font color='red'>Digits Only </font>").show().fadeOut("slow");
                   return false;
        }
       });

       
    });

     function getaddress(tehsil, n){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            type: "POST",
            url: "{{route('getaddress')}}",
            data: {tehsil:tehsil},
            success: function(result){
             console.log(result.data.get_division.division_name);
                if(result.error_code == '200'){
                    $('.district'+n).val(result.data.district);
                    $('.division_id'+n).val(result.data.division_id);
                    $('.division'+n).val(result.data.get_division.division_name);
                    // $('#'+button_id).prop('hidden',true);
                    // Swal.fire('District is '+result.data.district+' & Division is '+result.data.get_division.division_name,"", "success");
                }
                else{
                     Swal.fire(result.message,"", "error");

                }
            }
        });
    } 



    
    
</script>  
@endpush