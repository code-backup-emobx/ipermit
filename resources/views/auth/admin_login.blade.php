
<?php
 // echo bcrypt('cf_bist_circle@123'); 
 // echo bcrypt('cf_ferozpur@123'); 
 // echo bcrypt('cf_north_circle@123'); 
 // echo bcrypt('cf_south_circle@123'); 
 // echo bcrypt('ccf_plain_zone@123'); 
 // echo bcrypt('cwlw@123'); die();
?>

<!DOCTYPE html>
<html lang="en" >
    <!--begin::Head-->
    <head><base href="">
      
        <meta charset="utf-8"/>
        <title>iPermit </title>
        <meta name="description" content="Login page"/>
     
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <meta name="_token" content="{!! csrf_token() !!}" />
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>       
        <!--end::Fonts-->
        <!--begin::Page Custom Styles(used by this page)-->
        <link href="{{asset('assets/css/pages/login/classic/login-1.css')}}" rel="stylesheet" type="text/css"/>
        <!--end::Page Custom Styles-->
        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
        <!--end::Global Theme Styles-->
        <!--begin::Layout Themes(used by all pages)-->

        <link href="{{asset('assets/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/css/themes/layout/header/menu/light.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/css/themes/layout/brand/dark.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/css/themes/layout/aside/dark.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('css/extra.css')}}" rel="stylesheet" type="text/css" />
                <!--end::Layout Themes-->
        <!-- <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico"/> -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">

    </head>
    <!--end::Head-->

    <!--begin::Body-->
    <body  id="kt_body"  class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading"  >

        <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
            <!--begin::Aside-->
            <div class="login-aside d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-10" style="background:white url(/assets/media/bg/bg-12.png) repeat fixed;">
               <!--    ="background-color:#d8e9d8-->
                <!--begin: Aside Container-->
                <div class="d-flex flex-row-fluid flex-column justify-content-between">
                    <!--begin: Aside header-->
                    <a href="#" class="flex-column-auto mt-5 pb-lg-0 pb-10">
                        <img src="" class="max-h-70px" alt=""/>
                    </a>
                    <h1 class="font-size-h1 mb-5 text-black ml-5" style=""><b>
                            WELCOME</b></h1> 
                    <!--end: Aside header-->

                    <!--begin: Aside content-->
                    <div class="flex-column-fluid d-flex flex-column justify-content-center" >
                        <h1 class="font-size-h1 mb-5 text-black" style=""><b>
                            <br>iPermit </b></h1> 
                           
                            <!-- <h1 class="font-size-h1 mb-5 text-black" style="">
                                
                            </h1> -->
                        
                        <p class="font-weight-lighter text-black opacity-80">
                        </p>
                    </div>
                    <!--end: Aside content-->

                    <!--begin: Aside footer for desktop-->
                    <div class="d-none flex-column-auto d-lg-flex justify-content-between mt-10">
                        <div class="opacity-70 font-weight-bold text-black" style="">
                            &copy; 2021 Wild Life Department Punjab
                        </div>
                        <div class="d-flex">
                        </div>
                    </div>
                    <!--end: Aside footer for desktop-->
                </div>
                <!--end: Aside Container-->
            </div>
            <!--begin::Aside-->

            <!--begin::Content-->
            <div class="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden">
                <!--begin::Content header-->
                <div class="position-absolute top-0 right-0 text-right mt-5 mb-15 mb-lg-0 flex-column-auto justify-content-center py-5 px-10">
                    <span class="font-weight-bold text-dark-50"></span>
                    <a href="javascript:;" class="font-weight-bold ml-2" id="kt_login_signup"></a>
                </div>
                <!--end::Content header-->



                <!--begin::Content body-->
                <div class="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
                    <!--begin::Signin-->
                  

                    <div class="login-form login-signin">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1" style="">Sign In</h3>
                            <p class="text-muted font-weight-bold" style="">Enter your username and password</p>
                        </div>
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_signin_form1" action="/admin/login" method="post" method="post">
                            <div class="form-group form-actions">
                           <!-- <input type="button" name="pccf" id="pccf"  value="PCCF" class="btn pulse-btn green">
                           <input type="button" name="acs" id="acs"  value="ACS" class="btn pulse-btn green">
                           <input type="button" name="apccf" id="apccf"  value="APCCF" class="btn  --><!-- pulse-btn green"> -->
                           <input type="button" name="dfo" id="dfo"  value="DFO" class="btn btn-light-success">
                           <input type="button" id="ro" name="ro" value="RO" class="btn btn-light-success">
                           <!-- <input type="button" id="cfw" name="cf" value="CF/CCF" class="btn btn-light-success"> -->
                           <input type="button" id="cf" name="cf" value="CF/CCF" class="btn btn-light-success">
                          <!--  <input type="button" id="cfshivalik" name="cfshivalik" value="CF" class="btn btn-light-success"> -->
                           <!-- <input type="button" id="cfppa" name="cfppa" value="CF PPA" class="btn btn-light-success"> -->
                           <!-- <input type="button" name="super" value="CF" class="btn btn-light-success"> -->
                           
                           <input type="button" id="admin" name="" value="Superadmin" class="btn btn-light-success">
                        </div>
                        @csrf
                        <div class="form-group" id="superadmin" >
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                         <input type="text" class="form-control h-auto py-5 px-6" placeholder="Username" id="username" value="SiteAdmin" readonly />  
                        </div>

                        <div class="form-group"  style="display:none">
                            <input type="text" class="form-control h-auto py-5 px-6 username" value="cfw" readonly />  
                        </div>

                        <div class="form-group" id="cfshivalik_username" style="display:none">
                            <input type="text" class="form-control h-auto py-5 px-6 username" value="cfshivalik" readonly id="cfshivalik_name"/>  
                        </div>

                        <div class="form-group" id="cfppa_username" style="display:none">
                            <input type="text" class="form-control h-auto py-5 px-6 username" value="cfppa" readonly id="cfppa_name"/>  
                        </div>
                        
                        <div class="form-group"  id="dfoadmin" style="display:none">
                            <div class="input-icon">
                                <select id="division_name" class="form-control h-auto py-5 px-6">
                                 <option value="">Select Division</option>
                                   @if(!empty($divisions))
                                   <?php foreach($divisions as $division){ ?>
                                   <option value="{{$division->division_name}}">{{$division->division_name}}</option>
                                    <?php } ?>
                                   @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group"  id="cf_username" style="display:none">
                            <div class="input-icon">
                                <select id="cf_name" class="form-control h-auto py-5 px-6">
                                 <option value="">Select</option>
                                   @if(!empty($cf_ccf))
                                   <?php foreach($cf_ccf as $list){ ?>
                                   <option value="{{$list->name}}">{{$list->name}}</option>
                                    <?php } ?>
                                   @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group"  id="dfo_for_range" style="display:none">
                            <div class="input-icon">
                                <select id="division_value" class="form-control h-auto py-5 px-6">
                                 <option value="">Select Division</option>
                                   @if(!empty($divisions))
                                   <?php foreach($divisions as $division){ ?>
                                   <option value="{{$division->id}}">{{$division->division_name}}</option>
                                    <?php } ?>
                                   @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group"  id="roadmin" style="display:none">
                            <div class="input-icon">
                                <select id="ro_name" class="form-control h-auto py-5 px-6">
                                 <option value="">Select Range</option>
                                  
                                </select>
                            </div>
                        </div>
                         <div class="form-group"  id="dfoadminwl" style="display:none">
                            <div class="input-icon">
                                <select id="division_name_wl" class="form-control h-auto py-5 px-6">
                                 <option value="">Select Division (Wildlife)</option>
                                   @if(!empty($divisions_wl))
                                   <?php foreach($divisions_wl as $division_wl){ ?>
                                   <option value="{{$division_wl->division_name}}">{{$division_wl->division_name}}</option>
                                    <?php } ?>
                                   @endif
                                </select>
                            </div>
                        </div>
                            <div class="form-group">
                                <input class="form-control h-auto py-5 px-6" type="password" placeholder="Password" name="password" autocomplete="off" required/>
                            </div>
                            <!--begin::Action-->
                            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                                <!-- <a href="javascript:;" class="text-dark-50 text-hover-primary my-3 mr-2" id="kt_login_forgot">
                                    Forgot Password ?
                                </a> -->
                                <input type="submit" id="kt_login_signin_submit1" class="btn btn-primary font-weight-bold px-9 py-4 my-3"value="Sign In">
                            </div>
                            <!--end::Action-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signin-->

                    <!--begin::Signup-->
                    <div class="login-form login-signup">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1" >Sign Up</h3>
                            <p class="text-muted font-weight-bold">Enter your details to create your account</p>
                        </div>
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_signup_form">
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="text" placeholder="Fullname" name="fullname" autocomplete="off"/>
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="email" placeholder="Email" name="email" autocomplete="off"/>
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="password" placeholder="Password" name="password" autocomplete="off"/>
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="password" placeholder="Confirm password" name="cpassword" autocomplete="off"/>
                            </div>
                            <div class="form-group">
                                <label class="checkbox mb-0">
                                    <input type="checkbox" name="agree"/>
                                    <span></span>
                                    I Agree the <a href="#">terms and conditions</a>
                                </label>
                            </div>
                            <div class="form-group d-flex flex-wrap flex-center">
                                <button type="button" id="kt_login_signup_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</button>
                                <button type="button" id="kt_login_signup_cancel" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signup-->
                    <!--begin::Forgot-->
                    <div class="login-form login-forgot">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1">Forgotten Password ?</h3>
                            <p class="text-muted font-weight-bold">Enter your email to reset your password</p>
                        </div>
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_forgot_form">
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="email" placeholder="Email" name="email" autocomplete="off"/>
                            </div>
                            <div class="form-group d-flex flex-wrap flex-center">
                                <button type="button" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</button>
                                <button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Forgot-->
                </div>
                <!--end::Content body-->

                <!--begin::Content footer for mobile-->
                <div class="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
                    <div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">
                        &copy; 2021 Wild Life Department Punjab
                    </div>
                    <div class="d-flex order-1 order-sm-2 my-2">
                        <a href="#" style="color:gray" class="text-dark-75 text-hover-primary">Privacy</a>
                        <a href="#" style="color:gray" class="text-dark-75 text-hover-primary ml-4">Legal</a>
                        <a href="#" style="color:gray" class="text-dark-75 text-hover-primary ml-4">Contact</a>
                    </div>
                </div>
                <!--end::Content footer for mobile-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Login-->
    </div>
<!--end::Main-->
        <!--begin::Global Config(global config for global JS scripts)-->
        <script>
            var KTAppSettings = {
    "breakpoints": {
        "sm": 576,
        "md": 768,
        "lg": 992,
        "xl": 1200,
        "xxl": 1400
    },
    "colors": {
        "theme": {
            "base": {
                "white": "#ffffff",
                "primary": "#3699FF",
                "secondary": "#E5EAEE",
                "success": "#1BC5BD",
                "info": "#8950FC",
                "warning": "#FFA800",
                "danger": "#F64E60",
                "light": "#E4E6EF",
                "dark": "#181C32"
            },
            "light": {
                "white": "#ffffff",
                "primary": "#E1F0FF",
                "secondary": "#EBEDF3",
                "success": "#C9F7F5",
                "info": "#EEE5FF",
                "warning": "#FFF4DE",
                "danger": "#FFE2E5",
                "light": "#F3F6F9",
                "dark": "#D6D6E0"
            },
            "inverse": {
                "white": "#ffffff",
                "primary": "#ffffff",
                "secondary": "#3F4254",
                "success": "#ffffff",
                "info": "#ffffff",
                "warning": "#ffffff",
                "danger": "#ffffff",
                "light": "#464E5F",
                "dark": "#ffffff"
            }
        },
        "gray": {
            "gray-100": "#F3F6F9",
            "gray-200": "#EBEDF3",
            "gray-300": "#E4E6EF",
            "gray-400": "#D1D3E0",
            "gray-500": "#B5B5C3",
            "gray-600": "#7E8299",
            "gray-700": "#5E6278",
            "gray-800": "#3F4254",
            "gray-900": "#181C32"
        }
    },
    "font-family": "Poppins"
};
        </script>
        <!--end::Global Config-->

        <!--begin::Global Theme Bundle(used by all pages)-->
               <script src="{{url('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
               <script src="{{url('assets/plugins/custom/prismjs/prismjs.bundle.js')}}" type="text/javascript"></script> 
                <script src="{{url('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
                <!--end::Global Theme Bundle-->

                <!--begin::Page Scripts(used by this page)-->
                <script src="{{url('assets/js/pages/custom/login/login-general.js')}}" type="text/javascript"></script>
                        <!--end::Page Scripts-->
                <script type="text/javascript">
            $("#cf").on("click", function()
            {
                $('#cf_username').show();
                $('#cfppa_username').hide();
                $('#cfshivalik_username').hide();
                $('#superadmin').hide();
                $('#dfoadmin').hide();
                $('#dfoadminwl').hide();
                $('#dfo_for_range').hide();
                $('#roadmin').hide();
               
               
                $('#division_name').removeAttr('name');
                $('#division_name_wl').removeAttr('name');
                $('#ro_name').removeAttr('name');
                $('#username').removeAttr('name');
                $('#cfshivalik_name').removeAttr('name');
                $('#cfppa_name').removeAttr('name');
                $('#cf_name').attr('name','name');

                // $('#username').val('');
                // $('#cf_name').val('');
                // $('#cfshivalik_name').val('');
                // $('#cfppa_name').val('');
                $('#division_name').val('');
                $('#division_name_wl').val('');

            
            });
            $("#cfshivalik").on("click", function()
            {
                $('#cf_username').hide();
                $('#cfppa_username').hide();
                $('#cfshivalik_username').show();
                $('#superadmin').hide();
                $('#dfoadmin').hide();
                $('#dfoadminwl').hide();
                $('#dfo_for_range').hide();
                $('#roadmin').hide();
               
               
                $('#division_name').removeAttr('name');
                $('#division_name_wl').removeAttr('name');
                $('#ro_name').removeAttr('name');
                $('#username').removeAttr('name');
                $('#cfppa_name').removeAttr('name');
                $('#cf_name').removeAttr('name');
                $('#cfshivalik_name').attr('name','name');

                // $('#username').val('');
                // $('#cf_name').val('');
                // $('#cfshivalik_name').val('');
                // $('#cfppa_name').val('');
                $('#division_name').val('');
                $('#division_name_wl').val('');
            });
            $("#cfppa").on("click", function()
            {
                $('#cf_username').hide();
                $('#cfppa_username').show();
                $('#cfshivalik_username').hide();
                $('#superadmin').hide();
                $('#dfoadmin').hide();
                $('#dfoadminwl').hide();
                $('#dfo_for_range').hide();
                $('#roadmin').hide();
               
               
                $('#division_name').removeAttr('name');
                $('#division_name_wl').removeAttr('name');
                $('#ro_name').removeAttr('name');
                $('#username').removeAttr('name');
                $('#cfshivalik_name').removeAttr('name');
                $('#cf_name').removeAttr('name');
                $('#cfppa_name').attr('name','name');

                // $('#username').val('');
                // $('#cf_name').val('');
                // $('#cfshivalik_name').val('');
                // $('#cfppa_name').val('');
                $('#division_name').val('');
                $('#division_name_wl').val('');
            });

            $("#admin").on("click", function()
            {
                $('#cf_username').hide();
                $('#cfppa_username').hide();
                $('#cfshivalik_username').hide();
                $('#superadmin').show();
                $('#dfoadmin').hide();
                $('#dfoadminwl').hide();
                $('#dfo_for_range').hide();
                $('#roadmin').hide();
               
               
                $('#division_name').removeAttr('name');
                $('#division_name_wl').removeAttr('name');
                $('#ro_name').removeAttr('name');
                $('#username').attr('name','name');
                $('#cfshivalik_name').removeAttr('name');
                $('#cf_name').removeAttr('name');
                $('#cfppa_name').removeAttr('name');

                // $('#username').val('');
                // $('#cf_name').val('');
                // $('#cfshivalik_name').val('');
                // $('#cfppa_name').val('');
                $('#division_name').val('');
                $('#division_name_wl').val('');
            });
            $("#dfo").on("click", function(){
               $('#dfoadmin').show();
                $('#superadmin').hide();
                $('#dfoadminwl').hide();
                $('#dfo_for_range').hide();
               $('#roadmin').hide();
               $('#cfshivalik_username').hide();
               $('#cf_username').hide();
                $('#cfppa_username').hide();
               
               
                $('#username').removeAttr('name');
                $('#cfshivalik_name').removeAttr('name');
                $('#cf_name').removeAttr('name');
                $('#cfppa_name').removeAttr('name');
                $('#division_name_wl').removeAttr('name');
                $('#ro_name').removeAttr('name');
                $('#division_name').attr('name','name');


                // $('#username').val('');
                // $('#cf_name').val('');
                // $('#cfshivalik_name').val('');
                // $('#cfppa_name').val('');
                $('#division_name').val('');
                $('#division_name_wl').val('');
            });

            $("#ro").on("click", function(){
                $('#dfo_for_range').show();
                $('#roadmin').show();
                $('#superadmin').hide();
                $('#dfoadminwl').hide();
                $('#dfoadmin').hide();
                $('#cfshivalik_username').hide();
                $('#cf_username').hide();
                $('#cfppa_username').hide();
               
               
                $('#username').removeAttr('name');
                $('#cfshivalik_name').removeAttr('name');
                $('#cf_name').removeAttr('name');
                $('#cfppa_name').removeAttr('name');
                $('#division_name_wl').removeAttr('name');
                $('#division_name').removeAttr('name');
                $('#ro_name').attr('name','name');

                // $('#username').val('');
                // $('#cf_name').val('');
                // $('#cfshivalik_name').val('');
                // $('#cfppa_name').val('');
                $('#division_name').val('');
                $('#division_name_wl').val('');
            });
            
            $(document).ready(function(){
                $('#dfo_for_range').change(function(){
                    // alert('working');
                    var division_id = $('#dfo_for_range option:selected').val();
                    // alert(division_id);
                    
                    $.ajax({
                          method: 'POST', // Type of response and matches what we said in the route
                          headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                          url: "{{route('get_range')}}",
                         // This is the url we gave in the route
                          data: {'division_id' : division_id}, // a JSON object to send back
                          success: function(response){ // What to do if we succeed
                              console.log(response); 
                                $('#ro_name').html(response);

                          },
                          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                              console.log(JSON.stringify(jqXHR));
                              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                          }
                      });
                });
            });
        </script>
            </body>
    <!--end::Body-->
</html>
