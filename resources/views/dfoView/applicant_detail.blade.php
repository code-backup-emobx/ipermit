@extends('layouts.app2')

@section('content')

  <!-- <div class="d-flex flex-column-fluid"> -->
        <!--begin::Container-->
        <div class=" container ">
            <div class="card card-custom">
              <div class="card-body p-0">
        <!--begin: Wizard-->
               <div class="wizard wizard-2" id="kt_wizard_v2" data-wizard-state="step-first" data-wizard-clickable="false">

                 <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                <!--begin: Wizard Form-->
    <!-- starts wizards form-------------------------------------------------------s -->
                  
               <h1 class="text-green"><b>Applicant Form Apply for IPermit</b></h1>  
               <br>
               <!-- <h4>Permit For:
                  @if($applicant_detail_for_dfo->permit_type == 'crop_field')
                      &nbsp;Crop Field
                  @elseif($applicant_detail_for_dfo->permit_type == 'sarpunch')
                      &nbsp;Sarpanch
                  @else
                    &nbsp;Individual
                  @endif
              </h4>      -->
              <div class="row">       
              <div class="col-xl-12">            
          
                <div class="card">
                       
                  <div class="card-body table table-responsive">
                
                   <table class="s-text2" style="width:100%;">
                      <tbody>
                      <tr class="odd">
                        <th>Application Number</th>
                        <th>: &nbsp;{{ $applicant_detail_for_dfo->application_number}}</th>
                      </tr>

                      <tr class="even">
                        <th>Applicant Name</th>
                        <th>: &nbsp;{{ $applicant_detail_for_dfo->applicant_name }}</th>
                      </tr>
                     
                      <tr class="odd">
                        <th>Aadhar Number</th>
                        <th>  : &nbsp;{{ $applicant_detail_for_dfo->aadhar_number }}</th>
                      </tr>

                      <!-- <tr class="even">
                        <th>Residence Address</th>
                        <th>: &nbsp;{{ $applicant_detail_for_dfo->residence_address}}</th>
                      </tr> -->

                      <!-- <tr class="even">
                        <th>Aadhar Card</th>
                        <th>  </th>
                      </tr> -->
                     <!--  <tr class="odd">
                        <th> Applied date</th>
                        <th> : &nbsp;{{ $applicant_detail_for_dfo->date_of_issue }}</th>
                      </tr> -->

                      <tr class="even">
                        <th>District</th>
                        <th>: &nbsp;{{ $applicant_detail_for_dfo->district }}</th>
                      </tr>

                      <tr class="odd">
                        <th>Division</th>
                        <th>  : &nbsp;{{ $applicant_detail_for_dfo->getDivision->division_name }}</th>
                      </tr>
                     <!--  <tr>
                        <th class="text-left">
                          <a href="{{ url('aadhar_card/'.$applicant_detail_for_dfo->aadhar_copy)}}" target="_blank" class="btn btn-green ">View Aadhar Card</a>
                        </th>
                        <th class="text-right">
                        <a href="{{ url('fard_pdf/'.$applicant_detail_for_dfo->fard_pdf)}}" target="_blank" class="btn btn-green ">View Fard PDF</a></th>
                      </tr> -->

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            @if(!empty($owner_details))
            <!-- <h2 class="text-green m-5"><b> Owner Details</b></h2> -->
               @php
                $i=1;
               @endphp
               
                @foreach($owner_details as $list) 
                  <h4 class="mt-3">Owner Detail {{$i}}</h4>
                  
                  <div class="row">  
                  <div class="col-xl-12">            
                    <div class="card ">
                      <div class="card-body table table-responsive">
                        <table class="s-text2" style="width:100%;">
                          <tbody>

                            <tr class="even">
                              <th>Owner Name</th>
                              <th>: &nbsp;{{ $list->owner_name }}</th>
                            </tr>
                            <tr class="odd">
                              <th>Owner Father Name</th>
                              <th>: &nbsp;{{ $list->owner_father_name }}</th>
                            </tr>
                           
                            <tr class="even">
                              <th>Owner Signature</th>
                              <!-- <th>: &nbsp;{{ $list->owner_sign}}</th> -->
                            </tr>
                            <tr>
                              
                            </tr>
                            @php
                            $i++;
                            @endphp
                          </tbody>
                        </table><br></br>
                        <iframe src="{{env('APP_URL').$list->owner_sign}}" height="300px" width="500px"></iframe>
                      </div>
                    </div>
                  </div>
                  </div>
                @endforeach
                @endif
            <h2 class="text-green mt-5"><b> Documents</b></h2>
            <div class="row">       
              <div class="col-xl-12">            
        
                <div class="card ">
                       
                  <div class="card-body table table-responsive">
                  
                  @if($applicant_detail_for_dfo->aadhar_card)
                  <h4 class="mt-3">Aadhar Card</h4>
                  <iframe src="{{env('APP_URL').$applicant_detail_for_dfo->aadhar_card}}" height="300px" width="500px"></iframe>
                  @endif
                  @if($applicant_detail_for_dfo->fard_pdf != null)
                  <h4 class="mt-3">Fard PDF</h4>
                  <iframe src="{{env('APP_URL').$applicant_detail_for_dfo->fard_pdf}}" height="300px" width="500px"></iframe>
                  @endif
                  
                  <table class="s-text2" style="width:100%;">
                    <tbody>
                      @if($applicant_detail_for_dfo->dfo_status == 'Assign to DFO')
                      <td>
                        <button class="btn btn-success" data-toggle="modal" data-target="#survey">Approve</button> 
                      </td>
                      <td>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#modal_3">Reject
                        </button><br>Short Coming/Communication to Applicant
                      </td>
                      @elseif($applicant_detail_for_dfo->dfo_status == 'Rejected')
                        <td>
                          <button class="btn btn-success">Rejected by DFO</button> 
                        </td>
                      @elseif($applicant_detail_for_dfo->dfo_status == 'Accepted')
                        <td>
                          <button class="btn btn-success">Accepted by DFO and Mark to RO</button> 
                        </td>
                      @elseif($applicant_detail_for_dfo->dfo_status == 'Assign to CF')
                        <td>
                          <button class="btn btn-success">Assign to CF</button> 
                        </td>
                      @endif
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
            </div>
            
         </div>
       </div>
                     
     <!-- add code for testing------------------------------------------------------------- -->
    
         <!-- @if($applicant_detail_for_dfo->permit_type == 'crop_field')
           <div class="col-xl-12">  
                <div class="card card-custom">
                    <iframe src="{{ $applicant_detail_for_dfo->fard_pdf_crop_field }}" style="width:100%; height:500px;" frameborder="0">
                    </iframe>
                </div>
            </div>
            @endif
       -->
     
        <!-- add code for testing-------------------------------------------------------------- -->
</div>
<!-- add code for buttons of return acept starts -->
<div class="card-body table table-responsive">
     <!--  if open -->
                  
           <table id="example" class="display" style="width:100%">
            <tbody>
              <tr>
                

                <!-- @if($applicant_detail_for_dfo->dfo_status == 'Assign to DFO')
                  
                
                <td>
                    <a href="{{ url('/adminLogin/dashboard') }}"><button type="submit" class="btn btn-info">Back</button></a> 
                  </td>
                @elseif($applicant_detail_for_dfo->dfo_status == 'Rejected')
                  <td>
                    <button class="btn btn-success"> Rejected by DFO</button>
                  </td>
                @elseif($applicant_detail_for_dfo->dfo_status == 'accepted')
                  <td>
                    <button class="btn btn-success"> Accepted</button>
                  </td>
                  <td>
                    <a href="{{ url('/adminLogin/dashboard') }}"><button type="submit" class="btn btn-success">Back</button></a> 
                  </td>
                @endif -->
                
                <!-- <td>
                  <button class="btn btn-success approve_btn" data-toggle="modal" data-target="#approve" style="display: none;">Approve</button> 
                </td> -->
                
                
                <!-- <td>
                  <a href="{{ url('/adminLogin/dashboard') }}"><button type="submit" class="btn btn-success">Back</button></a> 
                </td> -->
              </tr>
           </tbody></table>
             <!--  if return -->
        
                                  
           </div>
<!-- add code for buttons of return accept ends -->
</div>
            </div>
     </div>
                </div>
        <!--end: Wizard-->
                    </div>
           </div>
       </div>
        <!--end::Container-->
<!--     </div> -->

<div class="modal fade" id="modal_3" tabindex="-1" role="dialog" aria-labelledby="modal_3" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modal_title_3">Reason</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i aria-hidden="true" class="ki ki-close"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="modal_body_3">
                                         
                                      <div class='from-row'>
                                      <form id="" action="{{ url('/admin/reject_status') }}" method="post"onsubmit="return validateForm()" name="myForm">
                                        @csrf
                                        <input type="hidden" name="dfo_status" value="Rejected">
                                        <input type="hidden" name="ro_status" value="Rejected">
                                      <div class="row">
                                        <div class="col-xl-6">
                                          <div class="form-group">
                                              <!-- <label><b>Descriptin of weapon</b><font color="red"> * </font></label> -->
                                              <select class="form-control form-control-solid" name="reason" id="select_reason">
                                                  <!-- <option value="">Select</option> -->
                                                  <option>--Select Please--</option>
                                                  <option value="short_coming">Short Coming</option>
                                                  <option value="reject_close">Reject &amp; Close</option>
                                                  
                                              </select>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-xl-6 issues" style="display: none;">
                                        <div class="form-group">
                                          <!-- <label>Inline Checkboxes</label> -->
                                          <div class="checkbox-inline">
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="fard"/>
                                                  <span></span>
                                                  Fard Issue
                                              </label>
                                              
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="address" />
                                                  <span></span>
                                                  Address Issue
                                              </label>
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="other" />
                                                  <span></span>
                                                  Other
                                              </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-xl-6 reason" style="display: none;">
                                        <div class='form-group'>
                                           <label>Give reason:</label>
                                           <textarea rows="4" cols="50" class='form-control form-control-lg' name='remarks'></textarea> 
                                       </div> 
                                      </div>
                                       
                                 

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary font-weight-bold" id="">Save</button>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>

     
</div>
      <div class="modal fade" id="survey" tabindex="-1" role="dialog" aria-labelledby="survey" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_title_3">Survey</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" id="modal_body_3">
                   <form action="{{ url('/admin/dfo_survey') }}" method="post" name="myForm">
                    @csrf
                    
                     
                  @foreach($question_list as $listing)
                    <div class="form-group ">
                      <label><b> {{$listing->question}}</b><font color="red"> * </font></label>
                      <div class="radio-inline question">
                          <label class="radio">
                              <input type="radio" name="question" value="yes" class="ques" />
                              <span></span>Yes
                          </label>
                          <label class="radio">
                              <input type="radio" name="question" value="no" class="ques" />
                              <span></span>No
                          </label>
                      </div>
                    </div>
                  @endforeach
                </div>

                <div class="form-group col-md-6 choose_range" style="display: none;">
                  
                      <select id="ro_name" class="form-control" name="range_id">
                       <option value="">Please Choose Range Office</option>
                        @foreach($range as $range_list)
                        <option value="{{$range_list->id}}">{{$range_list->range_name}}</option>
                        @endforeach
                      </select>
                  
                </div>
             

                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary font-weight-bold" id="show">Save</button>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>
   

                     

@endsection
@push('page-script')
<script type="text/javascript">
//   FormValidation.formValidation(
//     document.getElementById('check_survey'),
//     {
//         fields: {
//             species_count: {
//                 validators: {
//                     notEmpty: {
//                         message: 'Please Enter Number of animals to be hunted.'
//                     },
//                     numeric: {
//                       message: 'Please Enter numbers'
//                     },
                      
              
//                 }
//             },
//         },

//         plugins: {
//             trigger: new FormValidation.plugins.Trigger(),
//             // Bootstrap Framework Integration
//             bootstrap: new FormValidation.plugins.Bootstrap(),
//             // Validate fields when clicking the Submit button
//             submitButton: new FormValidation.plugins.SubmitButton(),
//           // Submit the form when all fields are valid
//             defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
//         }
//     }
// );
</script>
<script>
  // when DOM is ready
$(document).ready(function () {
     // Attach Button click event listener 
    $("#dfo_accept").click(function(){
     // alert('hlo');
         // show Modal
         $('#show_range_modal').modal('show');
    });
    $("#myBtn").click(function(){
         $('#myModal').modal('show');
    });

    $(".question").click(function(){
        
        var value = $("input[name='question']:checked").val();
        // alert(permit_value);
        if(value == 'yes')
        {
            $('#show').text('Send to RO');
            $('.choose_range').show();
            
        }else{
            $('.choose_range').hide();
            $('#show').text('Send to CF Deviation');
        }
    });
});

$(document).on('change','#select_reason', function(){
  // alert('working');
  var option=$(this).find('option:selected');
  var value = option.val();//to get content of "value" attrib
  var text = option.text();//to get <option>Text</option> content
  // alert(value);
  // alert(text);
  if(value == 'short_coming')
  {
    $('.reason').show();
    $('.issues').show();
  }else{
    $('.reason').show();
    $('.issues').hide();
    
  }
  
});

$(document).on('change','.select_validity', function(){
  // alert('working');
  var value=$(".select_validity:checked").val();
  if(value == 'custom')
  {
    $('.show_date').show();
    // var list=$('.weapons_validity_list').val();
    // // alert(list);
    // $('.default_date').val(list);
    // $('.show_date').hide();
  }else{
    $('.show_date').hide();
  }
  // alert(value);
  // alert(text);
});

$(document).on('change','.date', function(){
  // alert('working');
  var value=$(this).val();
  // alert(value);
  $('.default_date').val(value);
});

$(document).on('change','#done_survey', function(){
  $('.approve_btn').show();
});



  function reject_by_dfo(){

  
    if(confirm(" Are your sure you want to reject the application ?")){
      $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "/admin/status",
                            data: {id:id},
                            success: function(result){
                           
                                if(result.error_code == '200'){
                                Swal.fire(result.message,"", "success");
                                }
                                else{
                                Swal.fire(result.message,"", "error");
                                }
                            }
                                
                    });
    }
    $('#reject_by_dfo').submit();
  }




  function accept_by_dfo(id){

  
    if(confirm(" Are your sure you want to accept the application ?")){

      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        type: "POST",
        url: "/admin/status",
        data: {id:id},
        success: function(result)
        {
          if(result.error_code == '200'){
          Swal.fire(result.message,"", "success");
          window.location.reload();
          }
          else{
          Swal.fire(result.message,"", "error");
          }
        }
      });
    }
   
  }
</script>

<script>
  function set_appliaction_id_in_modal_box(id){
   $('#application_id_reject').val(id);
  }
</script>



<script>
// function validateForm() {
//   var x = document.forms["myForm"]["reason"].value;
//   if (x == "") {
//     alert("Please fill the reason");
//     return false;
//   }
// }
</script>
@endpush