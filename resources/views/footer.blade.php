</div>
    <!--end::Wrapper-->
    </div>
    <!--end::Page-->
    </div>
    <!--end::Main-->





    <!-- begin::User Panel-->
    <div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
        <!--begin::Header-->
        <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
            <h3 class="font-weight-bold m-0">
                Logout
                <small class="text-muted font-size-sm ml-2"></small>
            </h3>
            <a href="/" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
            <i class="ki ki-close icon-xs text-muted"></i>
        </a>
        </div>
        <!--end::Header-->

        <!--begin::Content-->
        <div class="offcanvas-content pr-5 mr-n5">
            <!--begin::Header-->
            <div class="d-flex align-items-center mt-5">
                <div class="symbol symbol-100 mr-5">
<!--                     <div class="symbol-label" style="background-image:url('assets/media/logos/logo_trans.png')">Welcome</div>
                    <i class="symbol-badge bg-success"></i> -->
                </div>
                <div class="d-flex flex-column">
                    @if(Auth()->user()->name == 'cfw')
                        <a class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">CWLW</a>
                    @elseif(Auth()->user()->name == 'cfshiwalik')
                        <a class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">CF Shiwalik</a>
                    @elseif(Auth()->user()->name == 'cfppa')
                        <a class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">CF PPA</a>
                    @else
                        <a class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{Auth()->user()->name}}</a>
                    @endif
                    <div class="text-muted mt-1">
                       
                    </div>
                    <div class="navi mt-2">
                        <a href="/" class="navi-item">
                        <span class="navi-link p-0 pb-2">
                        
                            <span class="navi-text text-muted text-hover-primary">{{Auth()->user()->email}}</span>
                        </span>
                    </a>

                 <form method="POST" id="logout-form" action="{{ route('admin.logout') }}">
                    @csrf
                             </form> 
         <a class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5" href="#"
         onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Sign Out') }} </a>
         {{-- <a class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5" href="{{ url('admin/login') }}" >Sign Out </a> ---}}

                    </div>
                </div>
            </div>
            <!--end::Header-->

            <!--begin::Separator-->
            <div class="separator separator-dashed mt-8 mb-5"></div>
            <!--end::Separator-->

            <!--begin::Nav-->
          
            <!--end::Nav-->

            <!--begin::Separator-->
            <div class="separator separator-dashed my-7"></div>
            <!--end::Separator-->

            <!--begin::Notifications-->
            <div></div>
            <!--end::Notifications-->
        </div>
        <!--end::Content-->
    </div>
    <!-- end::User Panel-->


    <!--begin::Quick Cart-->
    <div id="kt_quick_cart" class="offcanvas offcanvas-right p-10">
        <!--begin::Header-->
        <div class="offcanvas-header d-flex align-items-center justify-content-between pb-7">
            <h4 class="font-weight-bold m-0">
               
            </h4>
            <a href="/" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_cart_close">
            <i class="ki ki-close icon-xs text-muted"></i>
        </a>
        </div>
        <!--end::Header-->

        <!--begin::Content-->
        <div class="offcanvas-content"></div>
        <!--end::Content-->
    </div>
    <!--end::Quick Cart-->

    <!--begin::Quick Panel-->
    <div id="kt_quick_panel" class="offcanvas offcanvas-right pt-5 pb-10"></div>
    <!--end::Quick Panel-->

    <!--begin::Chat Panel-->
    <div class="modal modal-sticky modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header align-items-center px-4 py-3">
                        <div class="text-left flex-grow-1">
                            <!--begin::Dropdown Menu-->
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="svg-icon svg-icon-lg"><!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>                            </button>
                                <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-md">
                                    <!--begin::Navigation-->
                                    <ul class="navi navi-hover py-5">
                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-drop"></i></span>
            <span class="navi-text">New Group</span>
        </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-list-3"></i></span>
            <span class="navi-text">Contacts</span>
        </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-rocket-1"></i></span>
            <span class="navi-text">Groups</span>
            <span class="navi-link-badge">
                <span class="label label-light-primary label-inline font-weight-bold">new</span>
            </span>
        </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-bell-2"></i></span>
            <span class="navi-text">Calls</span>
        </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-gear"></i></span>
            <span class="navi-text">Settings</span>
        </a>
                                        </li>

                                        <li class="navi-separator my-3"></li>

                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-magnifier-tool"></i></span>
            <span class="navi-text">Help</span>
        </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="/" class="navi-link">
            <span class="navi-icon"><i class="flaticon2-bell-2"></i></span>
            <span class="navi-text">Privacy</span>
            <span class="navi-link-badge">
                <span class="label label-light-danger label-rounded font-weight-bold">5</span>
            </span>
        </a>
                                        </li>
                                    </ul>
                                    <!--end::Navigation-->
                                </div>
                            </div>
                            <!--end::Dropdown Menu-->
                        </div>
                        <div class="text-center flex-grow-1">
                           
                        </div>
                        <div class="text-right flex-grow-1">
                         
                        </button>
                        </div>
                    </div>
                    <!--end::Header-->

                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin::Scroll-->
                        <div class="scroll scroll-pull" data-height="375" data-mobile-height="300">
                            <!--begin::Messages-->
                            <div class="messages">
                                <!--begin::Message In-->
                                <div class="d-flex flex-column mb-5 align-items-start"></div>
                                <!--end::Message In-->

                                <!--begin::Message Out-->
                                <div class="d-flex flex-column mb-5 align-items-end"></div>
                                <!--end::Message Out-->

                                <!--begin::Message In-->
                                <div class="d-flex flex-column mb-5 align-items-start"></div>
                                <!--end::Message In-->

                                <!--begin::Message Out-->
                                <div class="d-flex flex-column mb-5 align-items-end"></div>
                                <!--end::Message Out-->

                                <!--begin::Message In-->
                                <div class="d-flex flex-column mb-5 align-items-start"></div>
                                <!--end::Message In-->

                                <!--begin::Message Out-->
                                <div class="d-flex flex-column mb-5 align-items-end"></div>
                                <!--end::Message Out-->

                                <!--begin::Message In-->
                                <div class="d-flex flex-column mb-5 align-items-start"></div>
                                <!--end::Message In-->

                                <!--begin::Message Out-->
                                <div class="d-flex flex-column mb-5 align-items-end"></div>
                                <!--end::Message Out-->
                            </div>
                            <!--end::Messages-->
                        </div>
                        <!--end::Scroll-->
                    </div>
                    <!--end::Body-->

                    <!--begin::Footer-->
                    <div class="card-footer align-items-center">
                        <!--begin::Compose-->
                        <textarea class="form-control border-0 p-0" rows="2" placeholder="Type a message"></textarea>
                        <div class="d-flex align-items-center justify-content-between mt-5">
                            <div class="mr-3">
                                <a href="/" class="btn btn-clean btn-icon btn-md mr-1"><i class="flaticon2-photograph icon-lg"></i></a>
                                <a href="/" class="btn btn-clean btn-icon btn-md"><i class="flaticon2-photo-camera  icon-lg"></i></a>
                            </div>
                            <div>
                                <button type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Send</button>
                            </div>
                        </div>
                        <!--begin::Compose-->
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end::Card-->
            </div>
        </div>
    </div>
    <!--end::Chat Panel-->

    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop">
        <span class="svg-icon"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Up-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1"/>
        <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span></div>
    <!--end::Scrolltop-->

    <!--begin::Sticky Toolbar-->
    
    <!--end::Sticky Toolbar-->
    <!--begin::Demo Panel-->
    <div id="kt_demo_panel" class="offcanvas offcanvas-right p-10"></div>
    <!--end::Demo Panel-->

    <script>
        var HOST_URL = "/";
    </script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
    "breakpoints": {
        "sm": 576,
        "md": 768,
        "lg": 992,
        "xl": 1200,
        "xxl": 1400
    },
    "colors": {
        "theme": {
            "base": {
                "white": "#ffffff",
                "primary": "#3699FF",
                "secondary": "#E5EAEE",
                "success": "#1BC5BD",
                "info": "#8950FC",
                "warning": "#FFA800",
                "danger": "#F64E60",
                "light": "#E4E6EF",
                "dark": "#181C32"
            },
            "light": {
                "white": "#ffffff",
                "primary": "#E1F0FF",
                "secondary": "#EBEDF3",
                "success": "#C9F7F5",
                "info": "#EEE5FF",
                "warning": "#FFF4DE",
                "danger": "#FFE2E5",
                "light": "#F3F6F9",
                "dark": "#D6D6E0"
            },
            "inverse": {
                "white": "#ffffff",
                "primary": "#ffffff",
                "secondary": "#3F4254",
                "success": "#ffffff",
                "info": "#ffffff",
                "warning": "#ffffff",
                "danger": "#ffffff",
                "light": "#464E5F",
                "dark": "#ffffff"
            }
        },
        "gray": {
            "gray-100": "#F3F6F9",
            "gray-200": "#EBEDF3",
            "gray-300": "#E4E6EF",
            "gray-400": "#D1D3E0",
            "gray-500": "#B5B5C3",
            "gray-600": "#7E8299",
            "gray-700": "#5E6278",
            "gray-800": "#3F4254",
            "gray-900": "#181C32"
        }
    },
    "font-family": "Poppins"
};
    </script>
    <!--end::Global Config-->

    <!--begin::Global Theme Bundle(used by all pages)-->

     <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script> 
     <script src="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.js')}}" type="text/javascript"></script> 
     <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script> 
     <script src="{{asset('assets\custom\table2excel.js')}}" type="text/javascript"></script> 

     <script>
function export_table_excel(tablesId)
 {  
         var f=Date.now();
   $("#"+tablesId).table2excel({
    filename: "table"+f+".xls",
     exclude: ".noExl",
   
  });
 }



    function change_status(id,status,modelLastName)
    { 
        
        var token = '{{ csrf_token() }}';
       if(confirm("Are you sure you want to "+status+" this record."))
       { 
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: "POST",
                url: "/change_status",
                data: {id:id,status:status,modelLastName:modelLastName},
                success: function(result){
                     $('#customerid_'+id).hide();
                     window.location.reload();                       
                }
          });
        }
    }  
    


</script>
   <script>
     var KTSummernoteDemo = function () {    
    // Private functions
    var demos = function () {
        $('.summernote').summernote({
            height: 150
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTSummernoteDemo.init();
});
 </script>

 <script>
    function numTwoDecimal(field) {
    setTimeout(function() {
        var regex = /\d*\.?\d?\d?\d?/g;
        field.value = regex.exec(field.value);
    }, );
}
</script> 

<script>
    function setSelectDivisionId(division_id) {

                      $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "/set_select_division_id",
                            data: {'division_id':division_id},
                            success: function(result){
                           
                               window.location.reload();
                            }
                                
                    });
}
</script>

    <!--end::Global Theme Bundle-->

    <!--begin::Page Scripts(used by this page)-->

     @stack('page-script')

    <!--end::Page Scripts--> 

   

</body>
<!--end::Body-->

</html>