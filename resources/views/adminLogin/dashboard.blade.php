 <?php

// echo bcrypt('feroze_44@54hgt');echo "<br>";
// echo bcrypt('hoshiy_8574@jyu');echo "<br>";
// echo bcrypt('pathan_586@8koe');echo "<br>";
 // echo bcrypt('patial_00@82mcb');echo "<br>";
 // echo bcrypt('philla_98@33ddd');echo "<br>";
 // echo bcrypt('rupnag_1@441ejh');echo "<br>";

 // echo bcrypt('admin@141');echo "<br>";
 // echo bcrypt('admin@141');echo "<br>";
 // echo bcrypt('developer@452');echo "<br>";
 // echo bcrypt('amri@7014');echo "<br>";
// echo bcrypt('bath@8575');echo "<br>";
// echo bcrypt('bhat*6587');echo "<br>";
// echo bcrypt('dasu&8828');echo "<br>";
// echo bcrypt('fero$9852');echo "<br>";
// echo bcrypt('gurd#2578');echo "<br>";
// echo bcrypt('hosh%2541');echo "<br>";
// echo bcrypt('jala%9810');echo "<br>";
// echo bcrypt('ludh&1001');echo "<br>";
// echo bcrypt('mans*1080');echo "<br>";
// echo bcrypt('moha(8000');echo "<br>";
// echo bcrypt('nawa)1005');echo "<br>";
// echo bcrypt('path#5008');echo "<br>";
// echo bcrypt('pati%6587');echo "<br>";
// echo bcrypt('pati%4568');echo "<br>";
// echo bcrypt('phil&8569');echo "<br>";
// echo bcrypt('rese#9823');echo "<br>";
// echo bcrypt('ropa%2501');echo "<br>";
// echo bcrypt('sang%7853');echo "<br>";
// echo bcrypt('muka&5858');echo "<br>";
// echo bcrypt('trai*4521');echo "<br>";
?>

 @extends('layouts.app2')
 @section('content') 
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.css"/>                  <!--begin::Content-->
         <div class="content  d-flex flex-column flex-column-fluid" id="kt_content" style="background:white url({{asset('assets/media/bg/bg-12.png')}}) repeat fixed;">
            <!--begin::Subheader-->
            <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
                <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                </div></div>

 <div class="d-flex flex-column-fluid" >
        <!--begin::Container-->
        <div class=" container ">
        
      @if(Auth::user()->role == 'admin')
                <div class="row">                <!--begin::Info-->
                    
    <div class="col-xl-12">
        <!--begin::Stats Widget 13-->

    <!--begin::Body-->
    <div class="card-body">
             
    <div class="text-inverse-primary font-weight-bolder font-size-h5 mb-2 mt-5">
    <select class="form-control"id="final_status_dropdown" onchange="set_select_final_status(this.value)">
    <option value='' @if(Session::get('final_status')=='') selected @endif>All Applications</option>
    <option value='1' @if(Session::get('final_status')=='1') selected @endif >Already Allowed to download certificate for applicant</option>
    <option value='9'  @if(Session::get('final_status')=='9') selected @endif>Pending To Allow Applications</option>
    </select>
    </div>
        <div class="font-weight-bold  font-size-sm">Certificate View Filter</div>
    </div>
    <!--end::Body-->

<!--end::Stats Widget 13-->
    </div>
        </div>
      @endif
      <form action="/adminLogin/dashboard" method="get" id="form1">
        <input type="hidden" name="status" value="" id="put_status">
      </form>
        
        <div class="row">                <!--begin::Info-->
                    
    <div class="col-xl-4">
        <!--begin::Stats Widget 13-->

<a onclick="get_data('last_7_days');" class="card card-custom bg-danger bg-hover-state-danger card-stretch gutter-b">
    <!--begin::Body-->
    <div class="card-body">
        <span class="svg-icon svg-icon-white svg-icon-3x ml-n1"><!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000"/>
    </g>
</svg><!--end::Svg Icon--></span>        <div class="text-inverse-primary font-weight-bolder font-size-h5 mb-2 mt-5"><!-- {{ $last_7_days ?? ''}} --></div>
        <div class="font-weight-bold text-inverse-primary font-size-sm"> Last 7 Days Open Applications </div>
    </div>
    <!--end::Body-->
</a>
<!--end::Stats Widget 13-->
    </div>
    <div class="col-xl-4">
        <!--begin::Stats Widget 14-->
<a onclick="get_data('Pending');" class="card card-custom bg-primary bg-hover-state-primary card-stretch gutter-b">
    <!--begin::Body-->
    <div class="card-body">
        <span class="svg-icon svg-icon-white svg-icon-3x ml-n1"><!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"/>
        <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3"/>
    </g>
</svg><!--end::Svg Icon--></span>        
    <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $all_open ?? ''}}</div>
        <div class="font-weight-bold text-inverse-success font-size-sm">All Open Application</div>
    </div>
    <!--end::Body-->
</a>
<!--end::Stats Widget 14-->
    </div>
    <div class="col-xl-4">
        <!--begin::Stats Widget 15-->

<a href="{{ url('/adminLogin/dashboard') }}" class="card card-custom bg-success bg-hover-state-success card-stretch gutter-b">
    <!--begin::Body-->
    <div class="card-body">
        <span class="svg-icon svg-icon-white svg-icon-3x ml-n1"><!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"/>
        <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"/>
        <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"/>
        <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"/>
    </g>
</svg><!--end::Svg Icon--></span>        <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{ $all ?? ''}}</div>
        <div class="font-weight-bold text-inverse-success font-size-sm">All Application</div>
    </div>
    <!--end::Body-->
</a>
<!--end::Stats Widget 15-->
    </div>
         </div>


    <div class="row">                <!--begin::Info-->
   
    <div class="col-xl-12">
        <!--begin::Stats Widget 14-->
    <div class="card card-custom">
    <!--begin::Body-->
    <div class="card-body table table-responsive">
 
                   <table id="example" class="table-hover display" style="width:100%">
                                            <thead>
                                                <tr></tr>
                                                   <th>S.No</th>
                                                   <th>Date</th>
                                                   <th>Application No.</th>
                                                   <th>Applicant Name</th>
                                                   <th>Division</th>
                                                   <!-- <th> Permit Type </th>  -->
                                                   <th> DFO Reply </th>
                                                   <th> RO Reply </th>
                                                   <th>Action</th>
                                                </tr>
                                            </thead>
                                            @if(!empty($applicant_data_for_dfo))
                                            @php
                                            $count = 1;

                                            @endphp

                                              @foreach($applicant_data_for_dfo as $details_applicant)

                                      @php
                                        $application_status= App\Models\applicationStatus::where('role_by','dfo')
                                        ->where('dfo_status','Rejected')
                                        ->where('applicant_permit_id', '$details_applicant->id')->first();
                                      @endphp
                                                <tr>
                                                  <th>{{$count++ }}</th>
                                                  <td>{{ $details_applicant->applied_date }}</td>
                                                  <td>{{ $details_applicant->application_number }}</td>
                                                  <td>{{ $details_applicant->applicant_name }}</td>
                                                  <td>{{ $details_applicant['getDivision']->division_name }}</td>
                                                  
                                                   <td>
                                                    @if($details_applicant->dfo_status == 'Rejected')
                                                    
                                                      <a class="btn btn-sm btn-light-danger" > Rejected by DFO </a>
                                                      <a class="btn btn-sm btn-light-primary" data-toggle="modal" data-target="#show_modal_{{$details_applicant->id}}"> Reason</a>
                                                      
                                                    @elseif($details_applicant->dfo_status == 'Accepted')
                                                      <a class="btn btn-sm btn-light-success" > Accepted </a>
                                                    @else
                                                      <a class="btn btn-sm btn-light-warning" > Pending </a>
                                                    @endif
                                                  </td> 
                                                  <td>
                                                    @if($details_applicant->ro_status ==null || $details_applicant->ro_status == 'Pending')
                                                      <a class="btn btn-sm btn-light-warning" > Pending</a>
                                                    @else
                                                      <a class="btn btn-sm btn-light-info" > {{$details_applicant->ro_status}}</a>
                                                    @endif
                                                  </td>
                                                  <td>
                                                      <a href="{{ url('/admin/view_applicant_detail/'.$details_applicant->id)}}" class="btn btn-sm btn-light-info" > View  </a>
                                                  </td> 
                                                </td>
                                                  </tr>
                                              @if($application_status != null)
                                              <div class="modal fade" id="show_modal_{{$details_applicant->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="modal_title_3">Report</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <i aria-hidden="true" class="ki ki-close"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                   
                                                                    <div class="col-xl-6">
                                                                        <div class='form-group'>
                                                                           <label>Reason:</label>
                                                                           <input type="text" name="" class='form-control form-control-lg' readonly value="{{$application_status->reason}}">
                                                                           
                                                                       </div> 
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class='form-group'>
                                                                           <label>Reason:</label>
                                                                           <input type="text" name="" class='form-control form-control-lg' readonly value="{{$application_status->issue}} issue">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class='form-group'>
                                                                           <label>Remarks:</label>
                                                                           <textarea rows="4" cols="50" class='form-control form-control-lg' name='remarks' readonly>{{$application_status->remarks}}</textarea> 
                                                                       </div> 
                                                                    
                                                                            
                                                                    </div>
                                                                        
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                                    </div>
                                                              </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif
                                                  </tbody>
                                              </table>
    </div>
    <!--end::Body-->
</div>
<!--end::Stats Widget 14-->
    </div>
  
         </div>

            </div>

               </div>
              
                </div>
            </div>
        </div>


      
 @endsection
 @push('page-script')
    <script src="assets/js/pages/widgets.js?v=7.0.6"></script>
 
   <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.js"></script> 

  
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
         "scrollX":        "200px",
        "scrollCollapse": false,
   dom: 'Bfrtip',
        buttons: [
           { 
              extend: 'excel',  
                exportOptions: {
                    columns: [1,2,3,4,5,6,7],

                }
            },
            { 
              extend: 'pdf',  
                exportOptions: {
                    columns: [1,2,3,4,5,6,7],

                }
            },
             { 
              extend: 'print',  
                exportOptions: {
                    columns: [1,2,3,4,5,6,7],

                }
            },
           
        ],

       
    });
     $('.buttons-html5').addClass('btn btn-light-info');
      $('.buttons-print').addClass('btn btn-light-info');
} );
    </script>
   <script>

  function get_data(status)
  {
    // alert(status);
    $('#put_status').val(status);
    $('#form1').submit();
  }

  function set_is_final(application_number) {
    if(confirm('After this action applicant can download the certificate. Are you sure to proceed ?')){
     $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "set_is_final",
                            data: {application_number:application_number},
                            success: function(result){
                            console.log(result);
                                if(result.error_code == '200'){
                                Swal.fire(result.message,"", "success");
                          
                                window.location.reload();
                              
                               

                                }
                                else{
                                Swal.fire(result.message,"", "error");
                                }
                            }
                                
                    });
      }
    } function set_select_final_status(final_status) {

     $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "set_select_final_status",
                            data: {final_status:final_status},
                            success: function(result){
                              window.location.reload();
                              
                            }
                                
                    });
      
    } 
</script>

 @endpush