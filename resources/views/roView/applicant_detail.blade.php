@extends('layouts.app2')

@section('content')

  <!-- <div class="d-flex flex-column-fluid"> -->
        <!--begin::Container-->
        <div class=" container ">
            <div class="card card-custom">
              <div class="card-body p-0">
        <!--begin: Wizard-->
               <div class="wizard wizard-2" id="kt_wizard_v2" data-wizard-state="step-first" data-wizard-clickable="false">

                 <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                <!--begin: Wizard Form-->
    <!-- starts wizards form-------------------------------------------------------s -->
               <h1 class="text-green"><b>Applicant Form Apply for IPermit</b></h1>  
               <br>
               <!-- <h4>Permit For:
                  @if($applicant_detail_for_ro->permit_type == 'crop_field')
                      &nbsp;Crop Field
                  @elseif($applicant_detail_for_ro->permit_type == 'sarpunch')
                      &nbsp;Sarpanch
                  @else
                    &nbsp;Air Force
                  @endif
              </h4>      -->
              <div class="row">       
              <div class="col-xl-12">            
          
                <div class="card">
                       
                  <div class="card-body table table-responsive">
                
                   <table class="s-text2" style="width:100%;">
                      <tbody>
                      <tr class="odd">
                        <th>Application Number</th>
                        <th>: &nbsp;{{ $applicant_detail_for_ro->application_number}}</th>
                      </tr>

                      <tr class="even">
                        <th>Applicant Name</th>
                        <th>: &nbsp;{{ $applicant_detail_for_ro->applicant_name }}</th>
                      </tr>
                     
                      <tr class="odd">
                        <th>Aadhar Number</th>
                        <th>  : &nbsp;{{ $applicant_detail_for_ro->aadhar_number }}</th>
                      </tr>

                      <tr class="even">
                        <th>District</th>
                        <th>: &nbsp;{{ $applicant_detail_for_ro->district }}</th>
                      </tr>

                      <tr class="odd">
                        <th>Division</th>
                        <th>  : &nbsp;{{ $applicant_detail_for_ro->getDivision->division_name }}</th>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            @if($owner_details)
            <h2 class="text-green m-5"><b> Owner Details</b></h2>
               @php
                $i=1;
               @endphp
               
                @foreach($owner_details as $list) 
                  <h4 class="mt-3">Owner Detail {{$i}}</h4>
                  
                  <div class="row">  
                  <div class="col-xl-12">            
                    <div class="card ">
                      <div class="card-body table table-responsive">
                        <table class="s-text2" style="width:100%;">
                          <tbody>

                            <tr class="even">
                              <th>Owner Name</th>
                              <th>: &nbsp;{{ $list->owner_name }}</th>
                            </tr>
                            <tr class="odd">
                              <th>Owner Father Name</th>
                              <th>: &nbsp;{{ $list->owner_father_name }}</th>
                            </tr>
                           
                            <tr class="odd">
                              <th>Owner Signature</th>
                              <!-- <th>: &nbsp;{{ $list->owner_sign}}</th> -->
                            </tr>
                            <tr>
                              
                            </tr>
                            @php
                            $i++;
                            @endphp
                          </tbody>
                        </table>
                        <iframe src="{{env('APP_URL').$list->owner_sign}}" height="300px" width="500px"></iframe>
                      </div>
                    </div>
                  </div>
                  </div>
                @endforeach
                @endif
            <h2 class="text-green mt-5"><b> Documents</b></h2>
            <div class="row">       
              <div class="col-xl-12">            
        
                <div class="card ">
                       
                  <div class="card-body table table-responsive">
                  
                  @if($applicant_detail_for_ro->aadhar_card)
                  <h4 class="mt-3">Aadhar Card</h4>
                  <iframe src="{{env('APP_URL').$applicant_detail_for_ro->aadhar_card}}" height="300px" width="500px"></iframe>
                  @endif
                  @if($applicant_detail_for_ro->fard_pdf != null)
                  <h4 class="mt-3">Fard PDF</h4>
                  <iframe src="{{env('APP_URL').$applicant_detail_for_ro->fard_pdf}}" height="300px" width="500px"></iframe>
                  @endif
                  
                  <table class="s-text2" style="width:100%;">
                    <tbody>
                      @if($applicant_detail_for_ro->ro_status == null || $applicant_detail_for_ro->ro_status == 'Pending')
                      <td>
                        <button class="btn btn-success" data-toggle="modal" data-target="#show_modal">Approveeee</button> 
                      </td>
                      <!-- <td>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#modal_3">Respond
                        </button><br>Short Coming/Communication to Applicant
                      </td> -->
                      @elseif($applicant_detail_for_ro->ro_status == 'Rejected')
                        <td>
                          <button class="btn btn-success">Rejected by RO</button> 
                        </td>
                      @elseif($applicant_detail_for_ro->ro_status == 'Accepted by RO & assign to DFO')
                        <td>
                          <button class="btn btn-success">Accepted by RO & Assign to DFO</button> 
                        </td>
                      
                      @endif
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

            
     <!-- add code for testing------------------------------------------------------------- -->
    
        <!--  @if($applicant_detail_for_ro->permit_type == 'crop_field')
           <div class="col-xl-12">  
                <div class="card card-custom">
                    <iframe src="{{ $applicant_detail_for_ro->fard_pdf_crop_field }}" style="width:100%; height:500px;" frameborder="0">
                    </iframe>
                </div>
            </div>
            @endif -->
      
     
        <!-- add code for testing-------------------------------------------------------------- -->
</div>
<!-- add code for buttons of return acept starts -->
<div class="card-body table table-responsive">
     <!--  if open -->
                  
          <!-- <table id="example" class="display" style="width:100%">
            <tbody>
              <tr>
                <td>
                  @if($applicant_detail_for_ro->ro_status == 'ro')
                    <button  id="ro_accept" class="btn btn-success" data-toggle="modal" data-target="#show_modal">
                    Verify &amp; Mark to DFO</button> 
                  @endif
                </td>
                <td>
                  <button class="btn btn-danger" data-toggle="modal" data-target="#modal_3" onclick="set_appliaction_id_in_modal_box()">Respond
                  </button>
                  Short Coming/Communication to Applicant
                </td>
                <td>
                  <a href="{{ url('/adminLogin/rohome') }}"><button type="submit" class="btn btn-success">Back</button></a> 
                </td>
              </tr>
            </tbody></table> -->
             <!--  if return -->
        
                                  
           </div>
<!-- add code for buttons of return accept ends -->
</div>
            </div>
     </div>
                </div>
        <!--end: Wizard-->
                    </div>
           </div>
       </div>
        <!--end::Container-->
<!--     </div> -->

        <!--begin::Modal to save invite or reminder-->
                        <div class="modal fade" id="modal_3" tabindex="-1" role="dialog" aria-labelledby="modal_3" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modal_title_3">Reason</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i aria-hidden="true" class="ki ki-close"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="modal_body_3">
                                         
                                      <div class='from-row'>
                                      <form id="" action="{{ url('/admin/reject_status') }}" method="post"onsubmit="return validateForm()" name="myForm">
                                        @csrf
                                        <input type="hidden" name="dfo_status" value="2">
                                        <input type="hidden" name="ro_status" value="rejected">
                                      <div class="row">
                                        <div class="col-xl-6">
                                          <div class="form-group">
                                              <!-- <label><b>Descriptin of weapon</b><font color="red"> * </font></label> -->
                                              <select class="form-control form-control-solid" name="reason" id="select_reason">
                                                  <!-- <option value="">Select</option> -->
                                                  <option>--Select Please--</option>
                                                  <option value="short_coming">Short Coming</option>
                                                  <option value="reject_close">Reject &amp; Close</option>
                                                  
                                              </select>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-xl-6 issues" style="display: none;">
                                        <div class="form-group">
                                          <!-- <label>Inline Checkboxes</label> -->
                                          <div class="checkbox-inline">
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="fard"/>
                                                  <span></span>
                                                  Fard Issue
                                              </label>
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="license" />
                                                  <span></span>
                                                  License Issue
                                              </label>
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="address" />
                                                  <span></span>
                                                  Address Issue
                                              </label>
                                              <label class="checkbox">
                                                  <input type="checkbox" name="issues[]" value="other" />
                                                  <span></span>
                                                  Other
                                              </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-xl-6 reason" style="display: none;">
                                        <div class='form-group'>
                                           <label>Give reason:</label>
                                           <textarea rows="4" cols="50" class='form-control form-control-lg' name='remarks'></textarea> 
                                       </div> 
                                      </div>
                                       
                                 

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary font-weight-bold" id="">Save</button>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!--end::Modal-->


                        
                    </div>
                       
<!-- Modal -->
<div class="modal fade" id="show_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_title_3">Range Office Survey</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" id="modal_body_3">
                     
                  <div class='from-row md-12'>
                  <form action="{{ url('/admin/ro_survey') }}" method="post" name="myForm">
                    @csrf
                    <input type="hidden" name="dfo_status" value="2">
                    <input type="hidden" name="ro_status" value="dfo">
                  
                    <div class="form-group ">
                      
                      <label><b>Total Station Report PDF </b><font color="red"> * </font></label>
                      <input type="file" class="form-control form-control-lg" name="station_report" accept="application/pdf"/>
                      <!-- <span class="form-text text-muted">Please upload the Pdf of Fard.</span> -->
                      
                    </div>
                    <div class="form-group ">
                      <label><b>KML file</b><font color="red"> * </font></label>
                      <input type="file" class="form-control form-control-lg" name="kml"/>
                      <!-- <span class="form-text text-muted">Please upload the Pdf of Fard.</span> -->
                    </div>
                    <div class="form-group ">
                      <div class="checkbox-inline">
                        <label class="checkbox">
                            <input type="checkbox" name="hammering" value="yes"/>
                            <span></span>
                            Chieseling and Hammering
                        </label>
                      </div>
                    </div>
                    
                  
                  
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary font-weight-bold" id="">Save</button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>

                        

                     

@endsection
@push('page-script')
<script>
  // when DOM is ready
$(document).ready(function () {
     // Attach Button click event listener 
    $("#ro_accept").click(function(){
     // alert('hlo');
         // show Modal
         $('#show_modal').modal('show');
    });
    
});
  function reject_by_dfo(){

  
    if(confirm(" Are your sure you want to reject the application ?")){
      $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "/admin/status",
                            data: {id:id},
                            success: function(result){
                           
                                if(result.error_code == '200'){
                                Swal.fire(result.message,"", "success");
                                }
                                else{
                                Swal.fire(result.message,"", "error");
                                }
                            }
                                
                    });
    }
    $('#reject_by_dfo').submit();
  }


  $(document).on('change','#select_reason', function(){
        // alert('working');
        var option=$(this).find('option:selected');
        var value = option.val();//to get content of "value" attrib
        var text = option.text();//to get <option>Text</option> content
        // alert(value);
        // alert(text);
        if(value == 'short_coming')
        {
          $('.reason').show();
          $('.issues').show();
        }else{
          $('.reason').show();
          $('.issues').hide();
          
        }
        
    });


  function accept_by_dfo(id){

  
    if(confirm(" Are your sure you want to accept the application ?")){

      $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            type: "POST",
                            url: "/admin/status",
                            data: {id:id},
                            success: function(result){
                           
                                if(result.error_code == '200'){
                                Swal.fire(result.message,"", "success");
                                window.location.reload();
                                }
                                else{
                                Swal.fire(result.message,"", "error");
                                }
                            }
                                
                    });
    }
   
  }
</script>

<script>
  function set_appliaction_id_in_modal_box(id){
   $('#application_id_reject').val(id);
  }
</script>



<script>
// function validateForm() {
//   var x = document.forms["myForm"]["reason"].value;
//   if (x == "") {
//     alert("Please fill the reason");
//     return false;
//   }
// }
</script>
@endpush